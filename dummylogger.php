<?php namespace Dummy\Logger;

/**
 * Dummy class used as facade when the VWIT logger library is not available
 * 
 * Add this class to the app/config/app.php
 * @author Aaldert van Weelden
 *
 */
class Logger{
	
	
		/**
		 * Indicates the dummy facade class is present
		 * @var boolean FALSE
		 */
		const EXISTS = false;
		
		public static $instance=null;

	
		private function __construct(){
	
		}
	
	
		public static function getInstance() {
			if(!isset(self::$instance)) {
				self::$instance = new Logger();
			}
			return self::$instance;
		}

		public static function log($tag=''){
			return self::getInstance();
		}
	
	
		public static function query($message='',$sender=''){
			
		}
	
		public static function trace($message='',$sender=''){
			
		}
	
		public static function dump($var='',$sender=''){
			
		}
	
		public static function debug($message='',$sender=''){
			
		}
	
		public static function info($message='',$sender=''){
			
		}
	
		public static function notice($message='',$sender=''){
			
		}
	
		public static function warn($message='',$sender=''){
			
		}
	
		public static function error($message='',$sender=''){
			
		}
	
}



?>