<?php namespace Foundation;
use Foundation\Utils\Util as Util;
/**
 * Class used for providing static utility methods for model and persistence related actions
 * @author Aaldert van Weelden
 *
 */
class PersistHelper{
	
	const TAG = 'PersistHelper';
	
	public static /*bit*/ function parseBit(/*string*/$in){
		
		if(is_int($in)){
			if($in==0){
				return 0;
			}
			return 1;
		}
		switch($in){
			case 'true':
				return 1;
				break;
			case 'false':
				return 0;
				break;
				
			case true:
				return 1;
				break;
			case false:
				return 0;
				break;
			default:
				if(Util::notNullOrEmptyOrZero($in)){
					return 1;
				}
				return 0;
		}
		$error='could not parse ['.$in.'] to a valid BIT value';
		throw new InvalidTypeException($error);	
	}
	
	/**
	 * Parse the integer value to the integer or NULL
	 */
	public static /*integer*/ function parseNullableInt(/*integer*/ $in){
		
		if(Util::nullOrEmpty($in)){
			return null;
		}
		
		return intval($in);
	}
	
	/**
	 * Parse the integer value to the integer or 0
	 */
	public static /*integer*/ function parseNotNullableInt(/*integer*/ $in){
	
		if(Util::nullOrEmptyOrZero($in)){
			return 0;
		}
	
		return intval($in);
	}
	
	
	
	/**
	 * Parse the float value to the float or NULL
	 */
	public static /*float*/ function parseNullableFloat(/*float*/ $in){
	
		if(Util::nullOrEmpty($in)){
			return null;
		}
	
		return floatval($in);
	}
	
	/**
	 * Parse the float value to the float or 0
	 */
	public static /*float*/ function parseNotNullableFloat(/*float*/ $in){
	
		if(Util::nullOrEmptyOrZero($in)){
			return 0;
		}
	
		return floatval($in);
	}
	
	/**
	 * Parse an object to a base64 encoded JSON string
	 * 
	 * @param object $obj  The object to parse
	 * @return NULL|string The result
	 */
	public static function parse2JSON_encodedString(/*object*/ $obj){
		if(Util::nullOrEmpty($obj)){
			return null;
		}
		
		$result = null;
		try {
			$result = json_encode($obj);
		} catch (Exception $e) {
			$result = null;
		}
		return base64_encode( $result  ) ;
	}
	
}




?>
