<?php namespace Foundation;

use Foundation\Utils\DateTimeUtil;
/**
 * Class used to implement JSON encoding and decoding of encapsuled child objects
 * @author Aaldert van Weelden
 *
 */
abstract class JSON{
	
	const TAG= 'JSON';
	
	private $headers;
	
	
	protected function __construct(){
		$this->headers = [
			
			'Cache-Control' => 'no-cache, must-revalidate',
			'Expires' => 'Mon, 26 Jul 1997 05:00:00 GMT',
			'Content-type' => 'application/json',
			'Date' => DateTimeUtil::getNowGMT(\DateTime::COOKIE),
			'Vary' => 'Accept-Encoding'
				
		];
	}
	
	/**
	 * Encode the object with child classes recursively to JSON format
	 */
	public function encodeJSON(){
		$json = new \stdClass();
		$self = $this->toArray();
		foreach ( $self as $key => $value){
			if(is_object($value) && method_exists($value,'toArray()')){
				
				$json->$key = $value->toArray();
			}else{
				$json->$key = $value;
			}
		}
		return json_encode($json);
	}
	
	
	/**
	 * Encode the object with child classes recursively to JSON format, wrapped in a data array
	 */
	public function encodeJSONasDataArray(){
		$json = new \stdClass();
		$self = $this->toArray();
		foreach ( $self as $key => $value){
			if(is_object($value) && method_exists($value,'toArray()')){
	
				$json->$key = $value->toArray();
			}else{
				$json->$key = $value;
			}
		}
		return array('data'=>json_encode($json));
	}
	
	
	/**
	 * Encode the object with child classes recursively to JSON format, nicely formatted suitable for logging
	 */
	public function encodeAndFormatJSON(){
		$json = new \stdClass();
		$self = $this->toArray();
		foreach ( $self as $key => $value){
			if(is_object($value) && method_exists($value,'toArray()')){
	
				$json->$key = $value->toArray();
			}else{
				$json->$key = $value;
			}
		}
		return $this->jsonFormat($json);
	}
	
	/**
	 * Encode the object with child classes recursively to base64 encoded JSON formatted string
	 */
	public function encodeBase64(){
	    return base64_encode($this->encodeJSON());
	}
	
	/**
	 * Convert object with child classes recursively to JSON object and output it to client with the appropriate headers
	 * @param Boolean  $moveOn If TRUE then move on, else EXIT. Default is TRUE
	 * @param Integer  $responseCode   The HTTP response code to send
	 * @param Array    $headers   The array with the headers to send. The array will be merged with the standard set of headers
	 * @param Boolean  $return   If TRUE the json response is returned as a string, Default is FALSE
	 */
	public function sendJSON($moveOn = true, $responseCode = null, $headers = [], $return = false){
		$json = new \stdClass();
		$self = $this->toArray();
		foreach ( $self as $key => $value){
			if(is_object($value) && method_exists($value,'toArray()')){
	
				$json->$key = $value->toArray();
			}else{
				$json->$key = $value;
			}
		}
		
		$content = json_encode($json);
		
		$responseHeaders = array_merge($this->headers, $headers);
		
		if($responseCode===null)$responseCode = HTTPcodes::HTTP_OK;

		http_response_code ( $responseCode );
		
		if (ob_get_length()) ob_end_clean();
		
		foreach ($responseHeaders as $key => $value){
			header($key.': '.$value);
		}
		header('Content-Length:', strlen($content));
		
		if($return) return $content;
		
		print $content;
		
		if(!$moveOn){
			exit;
		}
	}
	
	/**
	 * Convert object with child classes recursively to JSON object and output it to client with the appropriate headers as a JSONP response
	 * 
	 * @param 		   $callback   The callback used for the JSONP response execution
	 * @param Boolean  $moveOn If TRUE then move on, else EXIT. Default is TRUE
	 * @param Integer  $responseCode   The HTTP response code to send
	 * @param Array    $headers   The array with the headers to send. The array will be merged with the standard set of headers
	 * @param Boolean  $return   If TRUE the json response is returned as a string, Default is FALSE
	 * @param 
	 */
	public function sendJSONP($callback, $moveOn = true, $responseCode = null, $headers = []){
		$json = new \stdClass();
		$self = $this->toArray();
		foreach ( $self as $key => $value){
			if(is_object($value) && method_exists($value,'toArray()')){
	
				$json->$key = $value->toArray();
			}else{
				$json->$key = $value;
			}
		}
		
		$content = json_encode($json);
		
		$responseHeaders = array_merge($this->headers, $headers);
		
		if($responseCode===null)$responseCode = HTTPcodes::HTTP_OK;
		http_response_code ( $responseCode );
		if (ob_get_length()) ob_end_clean();
		
		foreach ($responseHeaders as $key => $value){
			header($key.': '.$value);
		}
		header('Content-Length:', strlen($content));
		
		if($return) return $callback.'('.$content.')';
		
		print $callback.'('.$content.')';
		
		if(!$moveOn){
			exit;
		}
	}
	
	public function decodeJSON($json_str)
	{
		//TODO fix this method
		$json = json_decode($json_str, false);
		foreach ($json as $key => $value)
		{
			$this->$key = $value;
		}
	}
	
	/**
	 * Parse the provided JSON string  and return it
	 * @param string $json
	 * @param string $type
	 * @return object Object
	 * 		The parsed object
	 */
	public function parseJSONstring($json){
		$object = null;
		if($json==null || empty($json)){
			$error="parseJSONObject failed, provided JSON string cannot be <null> or empty";
			Logger::error($error, self::TAG);
			throw new NullArgumentException($error);
		}
		if(!is_string($json)){
			$error="parseJSONObject failed, provided argument must be an instance of String";
			Logger::error($error, self::TAG);
			throw new InvalidArgumentException($error);
		}
	
		try{
			$object = json_decode($json);
		}catch(Exception $e){
			throw new Exception('parseJSONObject failed '.$e->getMessage());
		}
	
		return $object;
	
	}
	
	/**
	 * Convert the provided dto object to a valid json string
	 */
	public function toJSON($dto){
		$result = null;
	    if(Util::nullOrEmpty($dto)){
			$error="serialization failed, provided object cannot be <null> or empty";
			Logger::error($error, self::TAG);
			throw new NullArgumentException($error);
		}
		if(!is_object($dto)){
			$error="serialization failed, provided object must be an object";
			Logger::error($error, self::TAG);
			throw new InvalidArgumentException($error);
		}
		$result = json_encode($dto);
		return $result;
	}
	
	/**
	 * Format a flat JSON string to make it more human-readable
	*
	* @param string $json The original JSON string to process
	*        When the input is not a string it is assumed the input is RAW
	*        and should be converted to JSON first of all.
	* @return string Indented version of the original JSON string
	*/
	function jsonFormat($json) {
		
		if (!is_string($json)) {
			if (phpversion() && phpversion() >= 5.4) {
				return "\n\n".json_encode($json, JSON_PRETTY_PRINT);
			}
			$json = json_encode($json);
		}
		$result      = '';
		$pos         = 0;               // indentation level
		$strLen      = strlen($json);
		$indentStr   = "\t";
		$newLine     = "\n";
		$prevChar    = '';
		$outOfQuotes = true;
	
		for ($i = 0; $i < $strLen; $i++) {
			// Speedup: copy blocks of input which don't matter re string detection and formatting.
			$copyLen = strcspn($json, $outOfQuotes ? " \t\r\n\",:[{}]" : "\\\"", $i);
			if ($copyLen >= 1) {
				$copyStr = substr($json, $i, $copyLen);
				// Also reset the tracker for escapes: we won't be hitting any right now
				// and the next round is the first time an 'escape' character can be seen again at the input.
				$prevChar = '';
				$result .= $copyStr;
				$i += $copyLen - 1;      // correct for the for(;;) loop
				continue;
			}
	
			// Grab the next character in the string
			$char = substr($json, $i, 1);
	
			// Are we inside a quoted string encountering an escape sequence?
			if (!$outOfQuotes && $prevChar === '\\') {
				// Add the escaped character to the result string and ignore it for the string enter/exit detection:
				$result .= $char;
				$prevChar = '';
				continue;
			}
			// Are we entering/exiting a quoted string?
			if ($char === '"' && $prevChar !== '\\') {
				$outOfQuotes = !$outOfQuotes;
			}
			// If this character is the end of an element,
			// output a new line and indent the next line
			else if ($outOfQuotes && ($char === '}' || $char === ']')) {
				$result .= $newLine;
				$pos--;
				for ($j = 0; $j < $pos; $j++) {
					$result .= $indentStr;
				}
			}
			// eat all non-essential whitespace in the input as we do our own here and it would only mess up our process
			else if ($outOfQuotes && false !== strpos(" \t\r\n", $char)) {
				continue;
			}
	
			// Add the character to the result string
			$result .= $char;
			// always add a space after a field colon:
			if ($outOfQuotes && $char === ':') {
				$result .= ' ';
			}
	
			// If the last character was the beginning of an element,
			// output a new line and indent the next line
			else if ($outOfQuotes && ($char === ',' || $char === '{' || $char === '[')) {
				$result .= $newLine;
				if ($char === '{' || $char === '[') {
					$pos++;
				}
				for ($j = 0; $j < $pos; $j++) {
					$result .= $indentStr;
				}
			}
			$prevChar = $char;
		}
	
		return "\n\n".$result;
	}
	
}
?>