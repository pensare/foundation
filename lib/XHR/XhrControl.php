<?php namespace Foundation;
/**
 * Class used to encapsulate the paging grid controls
 * @author Aaldert van Weelden
 *
 */

class XhrControl {
    //sortDir
    const ASC = 'ASC';
    const DESC = 'DESC';
    
    //filtermode
    const OR_ = 'OR';
    const AND_ = 'AND';
    const XOR_ = 'XOR';
    
    public /*integer*/$currentPage;
    public /*integer*/$limit;
    public /*integer*/$total;
    public /*integer*/$page;
    public /*integer*/$lastPage;
    public /*integer*/$from;
    public /*integer*/$to;
    
    /**
     * Array of filter terms
     * @var array
     */
    public /*array*/$filter;
    /**
     * filtermode
     * @var Mode
     */
    public /*Mode*/$mode;
    /**
     * The sorting direction
     * @var SortDir
     */
    public /*SortDir*/$sortDir;
    
    private $constants;
    
    public function __construct(){
        $this->sortDir      = self::ASC;
        $this->mode         = self::OR_;
        $this->currentPage  = 1;
        $this->limit        = 10;
        $this->total        = 9999;
        $this->filter       = array();
        $this->page         = 0;
        $this->lastPage     = 0;
        $this->from         = 0;
        $this->to           = 0;
        
    }
}
?>