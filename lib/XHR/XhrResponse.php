<?php namespace Foundation;
use  Foundation\Utils\Util as Util;
/**
 * class used as XHR response object to wrap data and convert it to JSON string
 * @author Aaldert van Weelden
 *
 */

class XhrResponse extends JSON{
	
	const SUCCESS = true;
	const FAILURE = false;
	
	private /*String*/ $id;
	private /*String*/ $name;
	private /*String*/ $description;
	private /*integer*/ $code;
	private /*string*/ $sender;
	
	private /*Boolean*/ $success;
	private /*XhrError*/ $error;
	private /*String*/ $message;
	
	private /*Object*/ $data;
	private /*Object*/ $assets;
	public /*XhrControl*/ $controls;
	public /*String*/ $clazz;
	
	/**
	 * Constructor
	 * 
	 * @param String $name
	 * @param String $description
	 * @param String $success
	 * @param String $clazz
	 */
	public function __construct($name=null,$description=null,$success=true,$clazz=null){
		parent::__construct();
		
		$this->setName($name);
		$this->setDescription($description);
		$this->setSuccess($success);
		$this->id = Util::create_GUID();
		$this->data = new \StdClass;
		$this->controls = new XhrControl();
		$this->assets = new \StdClass;
		$this->clazz = $clazz;
	}
	
	public function getID(){
		return $this->id;
	}
	
	public function setID($id){
		$this->id=$id;
	}
	
	public function getData(){
		return $this->data;
	}
	
	public function setData($data){
		$this->data=$data;
	}
	
	public function getAssets(){
		return $this->assets;
	}
	
	public function setAssets($assets){
		$this->assets=$assets;
	}
	
	public function isSuccess(){
		return $this->success;
	}
	
	public function setSuccess($success){
		$this->success=$success;
	}
	
	public function getCode(){
		return $this->code;
	}
	
	public function setSender($sender){
		$this->sender=$sender;
	}
	
	public function getSender(){
		return $this->sender;
	}
	
	public function setCode($code){
		$this->code=$code;
	}
	
	public function getMessage(){
		return $this->message;
	}
	
	public function setMessage($message){
		$this->message=$message;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function setName($name){
		$this->name=$name;
	}

	public function getDescription(){
		return $this->description;
	}
	
	public function setDescription($description){
		$this->description=$description;
	}
	
	public function getControls(){
	    return $this->controls;
	}
	
	public function setControls($controls){
	    $this->controls = $controls;
	}
	
	/**
	 * Get the error 
	 */
	public function getError(){
		return $this->error;
	}
	
	/**
	 * Set the error 
	 * @param String $error
	 */
	public function setError($error){
		$this->error=$error;
	}
	
	/**
	 * Retrieves the merge of object en parent, used for cascading
	 * object to array conversion
	 */
	public function toArray() {
		return array_merge(get_object_vars($this));
	
	}
	
	
}

?>
