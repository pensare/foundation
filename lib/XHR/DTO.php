<?php namespace Foundation;

use Foundation\Utils\Util;
/**
 * The dto acts like a data transport object. This is a generic DTO class, can be used as a universal container
 * 
 * @author Aaldert van Weelden
 *
 */
abstract class DTO extends Reflectable{

	public function __construct(){

	}
	
	/* (non-PHPdoc)
	 * @see Reflectable::requestToDTO()
	 */
	public function requestToDTO($request){
		if(Util::nullOrEmpty($request)){
			$error="provided DTO object cannot be null";
			throw new NullArgumentException($error);
		}
		if(gettype($request)!="object"){
			$error="provided DTO object must be of type object, ".gettype($request)." given";
			throw new InvalidTypeException($error);
		}
	
		$dtoProperties = $this->getProperties();
		$requestProperties= array_keys(get_object_vars($request));
	
		$unsupported = array_diff($requestProperties,$dtoProperties);
	
		if(Util::notNullOrEmpty($unsupported)){
			$error = "Unsupported DTO fields detected: ".implode(',',$unsupported);
			throw new PropertyException($error);
		}
	
		foreach($dtoProperties as $key=>$property){
			if(in_array($property,$requestProperties)){
				$this->{$property} = $request->{$property};
			}
		}
	
		return $this;
	
	}
	
	/* (non-PHPdoc)
	 * @see Reflectable::equals()
	 */
	public function equals($obj){
	
		$dtoProperties = array();
		$objProperties = array();
		
		try {
			
			$dtoPropertyNames = (array) (new \ReflectionClass($this))->getProperties(\ReflectionProperty::IS_PUBLIC);
			$objPropertyNames = (array) (new \ReflectionClass($obj))->getProperties(\ReflectionProperty::IS_PUBLIC);
			
			foreach($dtoPropertyNames as $prop){
				$dtoProperties[$prop->name]= $this->{$prop->name};
			}
			foreach($objPropertyNames as $prop){
				$objProperties[$prop->name]= $obj->{$prop->name};
			}
		}
		catch (Exception $e) {
			return false;
		}
	
		if(    md5(implode('#',$dtoProperties)) === md5(implode('#',$objProperties))   ){
			return true;
		}
		return false;
	}
	
	
	/* (non-PHPdoc)
	 * example : $this->dto->findIn('bijlagedata.bestandsnaam', 'OSO-dossier.xml')->bestand
	 * @see Reflectable::find()
	 */
	public function findIn($key, $value){
		
		if($key==null || $value ==null){
			return null;
		}

		$root = null;
		$child = null;
		
		$propNames = explode(".", $key);
		if(count($propNames>0)){
			$root = $propNames[0];
		}
		
		if(count($propNames>1)){
			$child = $propNames[1];
		}
		
		if(is_array($this->{$root})){
			foreach ($this->{$root} as $item){
				if(in_array($value, $item)){
					return (object) $item;
				}
			}
		}
		return null;
	}
	
	
	public function toArray() {
		return array_merge(get_object_vars($this));
	
	}
	
	/**
	 * @return string
	 */
	public function __toString(){
		return get_class($this);
	}
}




?>