<?php namespace Foundation;
/**
 * Class used to define error codes
 * 
 * @author Aaldert van Weelden
 */
class ExceptionCode{
	
const AUTHENTICATION_ERROR = 50;
const DATABASE_ERROR = 100;
const PERSISTENCE = 150;
const OBJECT_NOT_FOUND = 170;
const LOCKED_ENTRY = 180;
const INVALID_TYPE = 200;
const INVALID_STRUCTURE = 210;
const KEY_INVALID = 220;
const STATUS_INVALID = 230;
const INVALID_ARGUMENT = 240;
const PROPERTY = 250;
const INVALID_COMPONENT = 300;
const NULL_ARGUMENT = 400;
const NOT_UNIQUE = 450;
const ACCESS_ERROR = 500;
const IMPORTER = 600;
const XML_PARSE_EXCEPTION = 610;
const INDEXABLE = 700;
const SOAPFAULT = 800;
const PARSE_ERROR = 900;



}

?>