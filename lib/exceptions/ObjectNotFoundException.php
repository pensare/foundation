<?php namespace Foundation;
/**
 * Persistence exception class
 * Throw it when an error is detected in the persistance layer
 * 
 * @author Aaldert van Weelden
 */
class ObjectNotFoundException extends \Exception{
	
	const TAG = "OBJECT NOT FOUND EXCEPTION: ";
	protected $message;
	protected $code;
	
	public function __construct($message, $code=0){
		$this->message=$message;
		if($code==0){
			$this->code=ExceptionCode::OBJECT_NOT_FOUND;
		}else{
		    $this->code=$code;
		}
		
		
		parent::__construct(self::TAG.$this->message, $this->code);
	}
	
	
	
	protected function setMessage($message){
		$this->message=$message;
	}
	
	protected function setCode($code){
		$this->code=$code;
	}
	
    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}


?>