<?php namespace Foundation;
/**
 * Property exception class
 * Throw it when a model class instance has no valid properties mapped to the database
 * 
 * @author Aaldert van Weelden
 */
class PropertyException extends \Exception{
	
	const TAG = "PROPERTY EXCEPTION: ";
	protected $message;
	protected $code;
	
	public function __construct($message, $code=0){
		$this->message=$message;
		if($code==0){
			$this->code=ExceptionCode::PROPERTY;
		}else{
		    $this->code=$code;
		}
		
		parent::__construct(self::TAG.$this->message, $this->code);
	}
	
	
	
	protected function setMessage($message){
		$this->message=$message;
	}
	
	protected function setCode($code){
		$this->code=$code;
	}
	
    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}


?>