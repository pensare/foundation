<?php namespace Foundation;
/**
 * KeyInvalid exception class
 * Throw it when a requested key does not exist
 * 
 * @author Aaldert van Weelden
 */
class InvalidKeyException extends \Exception{
	
	const TAG = "INVALID_KEY_EXCEPTION: ";
	protected $message;
	protected $code;
	
	public function __construct($message, $code=0){
		$this->message=$message;
		if($code==0){
			$this->code=ExceptionCode::KEY_INVALID;
		}else{
		    $this->code=$code;
		}
		
		
		parent::__construct(self::TAG.$this->message, $this->code);
	}
	
	
	
	protected function setMessage($message){
		$this->message=$message;
	}
	
	protected function setCode($code){
		$this->code=$code;
	}
	
    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}


?>