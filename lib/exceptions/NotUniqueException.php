<?php namespace Foundation;
/**
 * not unique exception class
 * 
 * @author Aaldert van Weelden
 */
class NotUniqueException extends \Exception{
	
	const TAG = "NOT_UNIQUE_EXCEPTION: ";
	protected $message;
	protected $code;
	
	public function __construct($message, $code=0){
		$this->message=$message;
		if($code==0){
			$this->code=ExceptionCode::NOT_UNIQUE;
		}else{
		    $this->code=$code;
		}
		
		
		parent::__construct(self::TAG.$this->message, $this->code);
	}
	
	
	
	protected function setMessage($message){
		$this->message=$message;
	}
	
	protected function setCode($code){
		$this->code=$code;
	}
	
    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}


?>