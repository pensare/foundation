<?php namespace Foundation;
/**
 * InvalidStatus exception class
 * Throw it when an entity has an illegal status
 * 
 * @author Aaldert van Weelden
 */
class InvalidStatusException extends \Exception{
	
	const TAG = "INVALID_STATUS_EXCEPTION: ";
	protected $message;
	protected $code;
	
	public function __construct($message, $code=0){
		$this->message=$message;
		if($code==0){
			$this->code=ExceptionCode::STATUS_INVALID;
		}else{
		    $this->code=$code;
		}
		
		
		parent::__construct(self::TAG.$this->message, $this->code);
	}
	
	
	
	protected function setMessage($message){
		$this->message=$message;
	}
	
	protected function setCode($code){
		$this->code=$code;
	}
	
    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}


?>