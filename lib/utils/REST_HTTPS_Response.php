<?php namespace Foundation\Utils;
use Foundation\XHR\JSON as JSON;
/**
 * class used as REST response object to wrap data and convert it to JSON string
 * @author Aaldert van Weelden
 *
 */

class REST_HTTPS_Response extends JSON{
	
	const VALID_RESULT  = "OK";
	
	private /*Object*/ $data;
	private /*Boolean*/ $success;
	private /*Boolean*/ $valid;
	private /*String*/ $message;
	private /*String*/ $name;
	//error code and message
	private /*String*/ $error;
	private /*String*/ $errorMessage;
	private /*String*/ $code;
	//optional, use this for returning the request parameters
	private /*String*/ $value;
	private /*String*/ $payload;
	private /*String*/ $id;
	
	/**
	 * Constructor
	 * 
	 * @param String $name
	 * @param String $value
	 * @param String $success
	 * @param String $valid
	 * @param String $message
	 * @param Object $data
	 */
	public function __construct($name=null,$value=null,$success=true,$valid=true,$message=null,$data=null){
		$this->setName($name);
		$this->setValue($value);
		$this->setSuccess($success);
		$this->setMessage($message);
		$this->setData($data);
		$this->setValid($valid);
	}
	
	public function getID(){
		return $this->id;
	}
	
	public function setID($id){
		$this->id=$id;
	}
	
	public function getData(){
		return $this->data;
	}
	
	public function setData($data){
		$this->data=$data;
	}
	
	public function getSuccess(){
		return $this->success;
	}
	
	public function setSuccess($success){
		$this->success=$success;
	}
	
	public function isValid(){
		return $this->valid;
	}
	
	public function setValid($valid){
		$this->valid=$valid;
	}
	
	
	public function getMessage(){
		return $this->message;
	}
	
	public function setMessage($message){
		$this->message=$message;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function setName($name){
		$this->name=$name;
	}

	public function getValue(){
		return $this->value;
	}
	
	public function setValue($value){
		$this->value=$value;
	}

	public function getPayload(){
		return $this->payload;
	}
	
	public function setPayload($payload){
		$this->payload=$payload;
	}
	
	
	/**
	 * Get the error 
	 */
	public function getError(){
		return $this->error;
	}
	
	/**
	 * Set the error 
	 * @param String $error
	 */
	public function setError($error){
		$this->error=$error;
	}
	
	/**
	 * Get the error i18n  message
	 */
	public function getErrorMessage(){
		return $this->errorMessage;
	}
	
	/**
	 * Set the error message
	 * @param String $errorMessage
	 */
	public function setErrorMessage($errorMessage){
		$this->errorMessage=$errorMessage;
	}
	
	/**
	 * Set the error i18n message
	 * @param String $errorMessage
	 */
	public function setI18nMessage($errorMessage){
		//i18n error message
		$messages = Messages::getInstance();//i18n
		$messages->create();
		$this->errorMessage=$messages->get($errorMessage);
	}
	
	/**
	 * Get the error code
	 */
	public function getCode(){
		return $this->code;
	}
	
	/**
	 * Set the error code
	 * @param String $code
	 */
	public function setCode($code){
		$this->code=$code;
		//i18n error message
		$messages = Messages::getInstance();//i18n
		$messages->create();
		$this->errorMessage=$messages->get($code);
	}
	
	/**
	 * Retrieves the merge of object en parent, used for cascading
	 * object to array conversion
	 */
	public function toArray() {
		return array_merge(get_object_vars($this));
	
	}
	
	
}

?>
