<?php
/** \file config.php
* Main configuration script.
*
* @author Aaldert van Weelden
* @date 23-07-2012
**/

//logging levels to write to debug.log
if (!defined('LOGLEVEL')) define('LOGLEVEL',$_ENV['log-level']);

if (!defined('LOG_QUERY')) define('LOG_QUERY',$_ENV['log-query']);

//logging exclusion filter
if( isset($_ENV['log-exclude']) ){
	if (!defined('LOGFILTER')) define('LOGFILTER',$_ENV['log-exclude']);
}else{
	if (!defined('LOGFILTER')) define('LOGFILTER',null);
}

/**
 * Environment specific configuration data
 *
 */
if( isset($_SERVER['REMOTE_ADDR']) ){
	if (!defined('REMOTE_ADRESS')) define("REMOTE_ADRESS",$_SERVER['REMOTE_ADDR']);
}else{
	if (!defined('REMOTE_ADRESS')) define("REMOTE_ADRESS",$_ENV['remote_adress']);
}

//log directory and logfiles
if (!defined('LOG_DIR')) define("LOG_DIR", $_ENV['log_dir']);
if (!defined('DEFAULT_LOGFILE_NAME')) define("DEFAULT_LOGFILE_NAME", $_ENV['DEFAULT_LOGFILE_NAME']);// The name of the log file for levels DEBUG and INFO
if (!defined('ERROR_LOGFILE_NAME')) define("ERROR_LOGFILE_NAME", $_ENV['ERROR_LOGFILE_NAME']);// The name of the log file for levels WARN and ERROR

if (!defined('QUERY_LOGFILE_NAME')) define("QUERY_LOGFILE_NAME", $_ENV['QUERY_LOGFILE_NAME']);// The name of the log file for the mySQL Queries

?>
