<?php
namespace Foundation\Utils;
/**
 * Logger class with backtrace and remote javascript logger (see also xhr/remotelogger.php)
 *
 * Use as static class:     Logger::debug( {String : the message} ) or as
 * class encapsulated logger:   $this->debug( {String : the message} , {the TAG});
 *
 *
 *
 * @author Aaldert van Weelden 2010
 *
 */
ini_set('html_errors', 0);
require_once 'config.php';
require_once 'Logger.php';


class LoggerInstance{
	
	private $tag;
	
	public function __construct($tag){
		$this->tag = $tag;
	}
	
	public function query($message,$tag=null){
	
		if($tag==null){
			$tag=$this->tag;
		}
		Logger::query($message,$tag);
	}

	public function trace($message,$tag=null){
		
		if($tag==null){
			$tag=$this->tag;
		}
		Logger::trace($message,$tag);
	}
	public function debug($message,$tag=null){
		if($tag==null){
			$tag=$this->tag;
		}
		Logger::debug($message,$tag);
	}
	public function info($message,$tag=null){
		if($tag==null){
			$tag=$this->tag;
		}
		Logger::info($message,$tag);
	}
	public function warn($message,$tag=null){
		if($tag==null){
			$tag=$this->tag;
		}
		Logger::warn($message,$tag);
	}
	/**
	 * Use the format Logger::log()->error(MESSAGE,ERROR,TAG) when used statically
	 * 
	 * @param string $message
	 * @param string $error
	 * @param string $tag
	 */
	public function error($message,$error=null,$tag=null){
		if($tag==null){
			$tag=$this->tag;
		}
		if($error!=null){
			if(is_a($error, 'Exception')){
				$message .= ': '.$error->getFile().' line '.$error->getLine().': '.$error->getMessage();
			}else{
				$message .= ': '.$error;
			}
		}
		Logger::error($message,$tag);
	}
	public function dump($var,$tag=null){
		if($tag==null){
			$tag=$this->tag;
		}
		Logger::dump($var,$tag);
	}
}
?>