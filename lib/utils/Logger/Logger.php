<?php
namespace Foundation\Utils;
/**
 * Logger class with backtrace and remote javascript logger (see also xhr/remotelogger.php)
 *
 * Use as static class:     Logger::debug( {String : the message} ) or as
 * class encapsulated logger:   $this->debug( {String : the message} , {the TAG});
 *
 *
 *
 * @author Aaldert van Weelden 2010
 *
 */
ini_set('html_errors', 0);
require_once 'config.php';

class LogSeverity{
	
	const TRACE 	= "TRACE";
	const DEBUG 	= "DEBUG";
	const INFO 		= "INFO";
	const WARN 		= "WARN";
	const ERROR 	= "ERROR";
	const DUMP 		= "DUMP";
	//php logging
	const PHPERROR 	= "PHPERROR";
	const PHPWARN 	= "PHPWARN";
	const PHPNOTICE ="PHPNOTICE";
	const PHPDEBUG 	= "PHPDEBUG";
	//query logging
	const QUERY 	= "QUERY";
	
}

class LogLevel{
	
	const TRACE 	= "TRACE";
	const VERBOSE 	= "VERBOSE";
	const DEBUG 	= "DEBUG";
	const INFO 		= "INFO";
	const WARN 		= "WARN";
	const ERROR 	= "ERROR";
	//php logging
	const NOTICE 	= "NOTICE";
}


class Logger{

	
	/**
	 * Indicates the logger facade class is present
	 * @var boolean TRUE
	 */
	const EXISTS = true;
	
	public static $instance=null;
	public static $loggerInstance=null;
    
	private function __construct(){

	}

	
	public static function getInstance() {
		if(!isset(self::$instance)) {
			self::$instance = new Logger();
		}
		return self::$instance;
	}
	
	/**
	 * Creates and returns the loggerInstance. Use this for static class logging, Logger::log(TAG)->debug(message)
	 * 
	 * @param string $tag
	 * @return \Utils\Logger\LoggerInstance
	 */
	private static function getLoggerInstance($tag) {
		if(!isset(self::$loggerInstance)) {
			self::$loggerInstance = new LoggerInstance($tag);
		}
		return self::$loggerInstance;
	}
	
	/**
	 * Returns a logger instance. Use this method for instantiated classes
	 * @param String $tag
	 */
	public function getLogger($tag=''){
		return self::getLoggerInstance($tag);
	}
	
	public static function log($tag=''){
		return self::getLoggerInstance($tag);
	}
	
	public static function create($tag=''){
		return self::getLoggerInstance($tag);
	}
	
	
	public static function query($message='',$sender=''){
		if(LOG_QUERY=='true'){
			self::formatMessage($message,LogSeverity::QUERY,$sender);
			return true;
		}
		else{
			return false;
		}
	}

	public static function trace($message='',$sender=''){
		if(LOGLEVEL==LogLevel::TRACE){
			self::formatMessage($message,LogSeverity::TRACE,$sender);
			return true;
		}
		else{
			return false;
		}
	}
	
	public static function dump($var='',$sender=''){
		if(LOGLEVEL==LogLevel::TRACE || LOGLEVEL==LogLevel::VERBOSE){
			$message=self::vardump($var);
			self::formatMessage($message,LogSeverity::DEBUG,$sender.' dump');
			return true;
		}
		else{
			return false;
		}
	}

    public static function debug($message='',$sender=''){
    	if(LOGLEVEL==LogLevel::TRACE || LOGLEVEL==LogLevel::VERBOSE || LOGLEVEL==LogLevel::DEBUG){
    		self::formatMessage($message,LogSeverity::DEBUG,$sender);
    		return true;
    	}
    	else{
    		return false;
    	}
    }

    public static function info($message='',$sender=''){
    	if(LOGLEVEL==LogLevel::TRACE || LOGLEVEL==LogLevel::VERBOSE || LOGLEVEL==LogLevel::DEBUG || 
    			LOGLEVEL==LogLevel::INFO){
    		self::formatMessage($message,LogSeverity::INFO,$sender);
    		return true;
    	}
    	else{
    		return false;
    	}
    }

    public static function notice($message='',$sender=''){
    	if(LOGLEVEL==LogLevel::TRACE || LOGLEVEL==LogLevel::VERBOSE || LOGLEVEL==LogLevel::DEBUG || 
    			LOGLEVEL==LogLevel::INFO || LOGLEVEL==LogLevel::NOTICE){
    		self::formatMessage($message,LogSeverity::NOTICE,$sender);
    		return true;
    	}
    	else{
    		return false;
    	}
    }

    public static function warn($message='',$sender=''){
    	if(LOGLEVEL==LogLevel::TRACE || LOGLEVEL==LogLevel::VERBOSE || LOGLEVEL==LogLevel::DEBUG || 
    			LOGLEVEL==LogLevel::INFO || LOGLEVEL==LogLevel::NOTICE || 
    			LOGLEVEL==LogLevel::WARN){
    		self::formatMessage($message,LogSeverity::WARN,$sender);
    		return true;
    	}
    	else{
    		return false;
    	}
    }

    public static function error($message='',$sender=''){
    	if(LOGLEVEL==LogLevel::TRACE || LOGLEVEL==LogLevel::VERBOSE || LOGLEVEL==LogLevel::DEBUG ||
    			 LOGLEVEL==LogLevel::INFO || LOGLEVEL==LogLevel::NOTICE || 
    			 LOGLEVEL==LogLevel::WARN || LOGLEVEL==LogLevel::ERROR){
    		self::formatMessage($message,LogSeverity::ERROR,$sender);
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    

    //==========remote javascript logging, has DEBUG,INFO,WARN and ERROR levels only================
    public static function remoteTrace($message='',$sender=''){
    	if(LOGLEVEL==LogLevel::TRACE){
    		self::formatRemoteMessage($message,LogSeverity::TRACE,$sender);
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    
    public static function remoteDebug($message='',$sender=''){
    	if(LOGLEVEL==LogLevel::TRACE ||LOGLEVEL==LogLevel::VERBOSE || LOGLEVEL==LogLevel::DEBUG){
    		self::formatRemoteMessage($message,LogSeverity::DEBUG,$sender);
    		return true;
    	}
    	else{
    		return false;
    	}
    }

    public static function remoteInfo($message='',$sender=''){
    	if(LOGLEVEL==LogLevel::TRACE ||LOGLEVEL==LogLevel::VERBOSE || LOGLEVEL==LogLevel::DEBUG || LOGLEVEL==LogLevel::INFO){
    		self::formatRemoteMessage($message,LogSeverity::INFO,$sender);
    		return true;
    	}
    	else{
    		return false;
    	}
    }


    public static function remoteWarn($message='',$sender=''){
    	if(LOGLEVEL==LogLevel::TRACE ||LOGLEVEL==LogLevel::VERBOSE || LOGLEVEL==LogLevel::DEBUG || 
    			LOGLEVEL==LogLevel::INFO || LOGLEVEL==LogLevel::NOTICE || LOGLEVEL==LogLevel::WARN){
    		self::formatRemoteMessage($message,LogSeverity::WARN,$sender);
    		return true;
    	}
    	else{
    		return false;
    	}
    }

    public static function remoteError($message='',$sender=''){
    	if(LOGLEVEL==LogLevel::TRACE ||LOGLEVEL==LogLevel::VERBOSE || LOGLEVEL==LogLevel::DEBUG || 
    			LOGLEVEL==LogLevel::INFO || LOGLEVEL==LogLevel::NOTICE || 
    			LOGLEVEL==LogLevel::WARN || LOGLEVEL==LogLevel::ERROR){
    		self::formatRemoteMessage($message,LogSeverity::ERROR,$sender);
    		return true;
    	}
    	else{
    		return false;
    	}
    }

    //uses reflection to trace the sender
    private static function get_log_sender($sender){
    	$backtrace = debug_backtrace();
    	//go back 2 or 3 levels
    	if($sender==''){
    		$index = 2;
    	}else{
    		$index = 3;
    	}

    	if(!isset($backtrace[3]) || $backtrace[3]==null){
    		$index=2;
    	}else if(!isset($backtrace[2]) || $backtrace[2]==null){
    		$index=1;
    	}

    	if($backtrace[$index]!=null && is_array($backtrace[$index])){
    		if(isset($backtrace[$index]['file'])){
    			$file = $backtrace[$index]['file'];
    		}else{
    			return 'no sender detected';
    		}
    		
    		if(isset($backtrace[$index]['line'])){
    			$line = $backtrace[$index]['line'];
    		}else{
    			return 'no sender detected';
    		}
    		
    		
    		if(strpos($file,'/')>-1){
    			$arr_file = explode('/',$file);
    		}else if(strpos($file,'\\')>-1){
    			$arr_file = explode('\\',$file);
    		}else{
    			return $file." line ".$line;
    		}
    		$count = count($arr_file);
    		if($count>1){
    			return $arr_file[$count-2].'/'. $arr_file[$count-1]." line ".$line;
    		}else if($count==1){
    			return $arr_file[0]." line ".$line;
    		}else{
    			return $file." line ".$line;
    		}

    		return $file." line ".$line;
    	}
    	return 'no sender detected';
    }

    /**
     * formats the error message in representable manner
     *
     * @param message this is the message to be formatted
     *
     * @return the formatted message
     */
    private static function formatMessage($message, $severity,$sender) {
    	
    	if($sender != null && is_string($sender) && !empty($sender) && $severity!=LogSeverity::ERROR && $severity!=LogSeverity::WARN ){
	    	if(strpos(strtoupper(LOGFILTER),strtoupper($sender)) !== false){
	    		return false;
	    	}
    	}
    	
    	$msg = date("Y-m-d H:i:s ") . " ";
    	
    	$msg .= str_pad(REMOTE_ADRESS,20);//IP
    	$msg .= str_pad($sender, 28);
    	$msg .= str_pad($severity,6);
    	$msg .= ": ";
    	
    	$sender = self::get_log_sender($sender);
    	$msg .= str_pad($sender,45);
    	$msg .= ": ";
    	
    	$msg .=  " ".$message . "\n";

    	switch($severity){
            case LogSeverity::ERROR:
            case LogSeverity::WARN:
                $logfile = fopen(LOG_DIR.ERROR_LOGFILE_NAME, "a");
                fputs($logfile, $msg);
                fclose($logfile);
            break;
            case LogSeverity::QUERY:
            	$logfile = fopen(LOG_DIR.QUERY_LOGFILE_NAME, "a");
            	fputs($logfile, $msg);
            	fclose($logfile);
            break;
            default:
                $logfile = fopen(LOG_DIR.DEFAULT_LOGFILE_NAME, "a");
                fputs($logfile, $msg);
                fclose($logfile);
                break;

        }
    	return true;
    }

    /**
     * formats the javascript remote error message in representable manner
     *
     * @param message this is the message to be formatted
     *
     * @return the formatted message
     */
    private static function formatRemoteMessage($message, $severity,$sender) {

    	$msg = date("Y-m-d H:i:s ") . " ";
    	
    	$msg .= str_pad(REMOTE_ADRESS,20);//IP
    	$msg .= str_pad('remoteJSlogger', 28);
    	$msg .= str_pad($severity,6);
    	$msg .= ": ";
    	 
    	$sender = 'jsClass '.$sender;
    	$msg .= str_pad($sender,45);
    	$msg .= ": ";
    	 
    	$msg .=  " ".$message . "\n";

    	switch($severity){
            case LogSeverity::ERROR:
            case LogSeverity::WARN:
                $logfile = fopen(LOG_DIR.ERROR_LOGFILE_NAME, "a");
                fputs($logfile, $msg);
                fclose($logfile);
            break;
            default:
                $logfile = fopen(LOG_DIR.DEFAULT_LOGFILE_NAME, "a");
                fputs($logfile, $msg);
                fclose($logfile);
                break;

        }

    	return true;
    }

    public static function vardump($var){
    	ob_start();
    	echo chr(10).chr(10);var_dump($var);echo chr(10);
    	$result = ob_get_contents();
    	ob_end_clean();
    	return $result;
   
    }
}
?>