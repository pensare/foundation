<?php 
/**
 * Include this file if usage of the project  .env file is not appropriate eg. in case of development or testing
 */




/*
 * enable query logging from Eloquent listener, see app/start/local.php
 */

  $_ENV['log-query'] = true;

  
  
 /*
  * logging levels to write to debug.log
  * allowed values are TRACE, VERBOSE,DEBUG,INFO,NOTICE,WARN,ERROR
  */
  
  $_ENV['log-level'] = 'TRACE';

  
  
 /*
  * logging filtering, add exclusion TAG comma-separated format, case insensitive
  */
  
  $_ENV['log-exclude'] = 'ServiceProvider,App::before, DomainRouter,AuthenticationService,ElementHelper,JsonHelper,TranslationHelper';
 


  
  
  $_ENV['remote_adress'] = '127.0.0.1';
  
  $_ENV['log_dir'] = '/logs/';
  
  $_ENV['DEFAULT_LOGFILE_NAME'] = 'myproject-info-debug.log';
  
  $_ENV['ERROR_LOGFILE_NAME'] = 'myproject-error-warn.log';
  
  $_ENV['QUERY_LOGFILE_NAME'] = 'myproject-query.log'

?>