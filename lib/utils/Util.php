<?php
namespace Foundation\Utils;
use Foundation\Utils\StringConstant as StringConstant;
/**
 * global utitliy helper functions
 * 
 * @author Aaldert van Weelden
 */
class Util{
	
	const TAG = 'Util';
	private static $instance;
	private static $dumpCount=0;
	private static $instanceCount=0;
	
	private function __construct(){
		
	}
	
	public static function getInstance() {
		if(!isset(self::$instance)) {
			self::$instance = new Util();
		}
		$instanceCount++;
		return self::$instance;
	}
	
	/**
	 * Maak een unique sleutelwaarde.
	 */
	public static function create_GUID() {
		$microTime = microtime();
		list($a_dec, $a_sec) = explode(" ", $microTime);
	
		$dec_hex = sprintf("%x", $a_dec* 1000000);
		$sec_hex = sprintf("%x", $a_sec);
	
		self::ensure_length($dec_hex, 5);
		self::ensure_length($sec_hex, 6);
	
		$guid = "";
		$guid .= $dec_hex;
		$guid .= self::create_guid_section(3);
		$guid .= '-';
		$guid .= self::create_guid_section(4);
		$guid .= '-';
		$guid .= self::create_guid_section(4);
		$guid .= '-';
		$guid .= self::create_guid_section(4);
		$guid .= '-';
		$guid .= $sec_hex;
		$guid .= self::create_guid_section(6);
		return $guid;
	}
	
	public static function create_guid_section($characters) {
		$return = "";
		for($i=0; $i<$characters; $i++) {
			$return .= sprintf("%x", mt_rand(0,15));
		}
		return $return;
	}
	
	public static function ensure_length(&$string, $length) {
		$strlen = strlen($string);
		if($strlen < $length) {
			$string = str_pad($string,$length,"0");
		}
		else if($strlen > $length){
			$string = substr($string, 0, $length);
		}
	}
	
	/**
	 * Create a custum password
	 *
	 */
	public static function create_password() {
		$length    = 7;
		$key_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$rand_max  = strlen($key_chars) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand_pos  = rand(0, $rand_max);
			$rand_key[] = $key_chars{$rand_pos};
		}
		$rand_pass = implode('', $rand_key);
		return ($rand_pass);
	}
	
	
	/**
	 * Create a random numberstring
	 *
	 */
	public static function create_number($length = 7) {
		
		$key_chars = '123456789123456789123456789123456789';
		$rand_max  = strlen($key_chars) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand_pos  = rand(0, $rand_max);
			$rand_key[] = $key_chars{$rand_pos};
		}
		$rand_pass = implode('', $rand_key);
		return ($rand_pass);
	}
	
	
	
	
	
    /* 
    * USE WITH JAVASCRIPT UTIL FUNCTION CLIENTSIDE
	* safeURI 		: 
    * @description encodes uri-component to base64 decoded JSON-object for sending as url
    * @method safeURI
    * @public
    * @static
    * @param {string}  uri
    * @return {string} decoded object
	*/
	
	/**
	 * javascript methods:
	 * 
	 * safeURI : function(uri,mode){
            typeof(mode)==='undefined'||mode==='' ||!mode ? mode='false':void(0);
			uri=this.URLtoObj(uri);//make object
			uri=_JSON.stringify(uri);//object to string
            switch(mode){
                case 'base64':
                    uri=_base64.encode(uri);//base64encode
                    break;
                case 'base64safe':
                    uri=_base64.encode(uri,true);//base64encode
                    break;
            }
			return uri;
		},
		
		safeObjToURI : function(obj,mode){
			typeof(mode)==='undefined'||mode==='' ||!mode ? mode='base64':void(0);
			var uri=_JSON.stringify(obj);//object to string
			switch(mode){
	            case 'base64':
	                uri=_base64.encode(uri);//base64encode
	                break;
	            case 'base64safe':
	                uri=_base64.encode(uri,true);//base64encode
	                break;
	        }
			return uri;
		},
	 * 
	 */
    

    public static function uriToJSONstring($data,$sEncode=false){
            switch($sEncode){
                case 'base64':
                    $data=base64_decode($data);
                    $data=utf8_encode($data);
                    break;
                case 'base64safe':
                    $data=base64_decode($data);
                    $data=Util::charcodeToTxt($data);
                    $data=utf8_encode($data);
                    break;
                default :
                    //
                    break;
                
            }
            $data=urldecode($data);
            
            return $data;
            
    }
    
    public static function uriToObject($data,$sEncode=false){
    	switch($sEncode){
    		case 'base64':
    			$data=base64_decode($data);
    			$data=utf8_encode($data);
    			break;
    		case 'base64safe':
    			$data=base64_decode($data);
    			$data=Util::charcodeToTxt($data);
    			$data=utf8_encode($data);
    			break;
    		default :
    			//
    			break;
    
    	}
    	$data=urldecode($data);
    
    	return json_decode($data);
    
    }
    
    
	
	public static function charcodeToTxt($value){
		$result='';
	
		$arr = explode('&', $value);
		foreach($arr as $key=>$val){
	
			$sub=explode(';', $val);
			isset($sub[0])?$one=$sub[0]:$one='';
			isset($sub[1])?$two=$sub[1]:$two='';
	
			if( strstr($one,'#') && strlen($one)>1 ){
				$code=intval( str_replace('#','',$sub[0]) );
				if($code>0){
					//var_dump($code)
					$one=chr($code);
				}
			}
			$result.=$one.$two;
		}
		return $result;
	}
	
	
	public static function libxml_display_errors() {
		$errors = libxml_get_errors();
		$err='';
		foreach ($errors as $error) {
			$err.=self::libxml_display_error($error);
		}
		libxml_clear_errors();
		return $err;
	}
	
	
	public static function nullOrEmpty($in=null,$zeroAllowed = true){
			if(!isset($in)) return true;
			if($zeroAllowed){
				if($in===0 || $in==='0') return false;
			}else{
				if($in===0 || $in==='0') return true;
			}
			
			if($in===null || $in==='null' || $in==='NULL') return true;
			
			if($in===false ||  $in==='false' ||  $in==='FALSE') return true;
			
			if($in==='undefined' ||  $in==='UNDEFINED') return true;
			
			if(empty($in)) return true;
			
			if($in=="") return true;
		
			if(is_string($in) && strlen($in)==0) return true;
			if(is_object($in) && count( (array) $in)==0) return true;
			//if(is_numeric($in) && $in==0) return true;
			
			return false;
		
	}
	
	public static function notNullOrEmpty($in=null){
		return !self::nullOrEmpty($in,true);
	}
	
	public static function notNullOrEmptyOrZero($in=null){
		return !self::nullOrEmpty($in,false);
	}
	
	public static function nullOrEmptyOrZero($in=null){
		return self::nullOrEmpty($in,false);
	}

	public static function parseNullOrEmpty($in=null){
		
		return self::nullOrEmpty($in)?null:$in;
	}
	
	/**
	 * Return TRUE if the input isNotNull AND input is valid array AND input size > 0
	 * @param array $in
	 * @return boolean
	 */
	public static function isValidArray($in=array()){
		return (self::notNullOrEmpty($in) && is_array($in) && count($in)>0);
	}
	
	
	public static function br2nl($s, $useReturn=false) {
	    return preg_replace('/(<br ?\/?>)/i', (($useReturn) ? '\r' : '\n'), $s);
	}
	
	public static function noBreak($s) {
		return preg_replace('/(<br ?\/?>)/i', '', $s);
	}
	
	
	/**
	 * return the underscore version of a camelcase name : CamelCase => camel_case
	 * @param String $name
	 */
    public static function camelCaseToUnderScore($name) {
    	
		return strtolower(preg_replace(
				array('/\\\\/', '/(?<=[a-z])([A-Z])/', '/__/'),
				array('_', '_$1', '_'),
				ltrim($name, '\\')
		));
	}
	
	
	public static function RemoveDir($dir, $DeleteMe) {
	
		if(!$dh = @opendir($dir)) return;
	
		while (false !== ($obj = readdir($dh))) {
	
			if($obj=='.' || $obj=='..') continue;
			if (!@unlink($dir.'/'.$obj)) {RemoveDir($dir.'/'.$obj, true);}
		}
	
		closedir($dh);
	
		if ($DeleteMe){
			@rmdir($dir);// verwijder dir
		}
	}
	
	
	public static function getCaller($html=true){
		if(LOGLEVEL=='VERBOSE' || LOGLEVEL=='DEBUG'){
			$e = new Exception();
			$trace = $e->getTrace();
			//position 0 would be the line that called this function so we ignore it
			$last_call = $trace[1];
			$type = null;
			$sep = null;
			switch($last_call['type']){
				case '->':
					$type="object scope [->]";
					$sep = '->';
					break;
				case '::':
					$type='static scope [::]';
					$sep = '::';
					break;
					
			}
			$last_call['type']=$type;
			
			$out = "\n<b>Methode ".$last_call['class'].$sep.$last_call['function']."() is called from :</b>\n";
			
			
			$out.= "\t--> file     : ".$last_call['file'].", line : ".$last_call['line']."\n";
			$out.= "\t--> function : ".$last_call['function']."\n";
			$out.= "\t--> class    : ".$last_call['class']."\n";
			$out.= "\t--> type     : ".$last_call['type']."\n";
			$out.= "\t--> args     : ".implode(",",$last_call['args'])."\n";
			
			if(!$html){
			   return $out;
			}
			print '<div style="font-family:Courier New, Courier, monospace;font-size:12px;">';
			print nl2br($out);
			print '</div>';
			return;
		}
		
	}
	
	/**
	 * Helper to dump the content to screen, can be expanded and collapsed
	 * @param object $object
	 * @param string $caption
	 * @param boolean $halt
	 */
	public static function dump($object, $caption=null, $halt=false){
		self::$dumpCount++;
		$ID = self::$dumpCount.'-'.self::$instanceCount;
		print '<br>';
		print '<span style="display:visible;cursor:pointer;" id="plus_'.$ID.'" onclick="
			document.getElementById(\'plus_'.$ID.'\').style.display=\'none\';
			document.getElementById(\'minus_'.$ID.'\').style.display=\'inline\';
			document.getElementById(\'dump_'.$ID.'\').style.display=\'block\';
		">[+]&nbsp;</span>';
		print '<span style="display:none;cursor:pointer;" id="minus_'.$ID.'" onclick="
			document.getElementById(\'plus_'.$ID.'\').style.display=\'inline\';
			document.getElementById(\'minus_'.$ID.'\').style.display=\'none\';
			document.getElementById(\'dump_'.$ID.'\').style.display=\'none\';
		">[-]&nbsp;</span>';
		if($caption!=null){
			print '<b>'.$caption.' : </b><br>';
		}
		print '<span style="display:none;" class="util_dump" id="dump_'.$ID.'">';
		print StringConstant::PR;
		var_dump($object);
		print StringConstant::PE;
		print '</span>';
		if($halt){
			exit();
		}
	}
	
	/**
	 * Helper to dump the content to screen, not collapsed
	 * @param object $object
	 * @param string $caption
	 * @param boolean $halt
	 */
	public static function dumpAndShow($object, $caption=null, $halt=false){
		self::$dumpCount++;
		$ID = self::$dumpCount.'-'.self::$instanceCount;
		print '<br>';
		if($caption!=null){
			print '<b>'.$caption.' : </b><br>';
		}	
		print StringConstant::PR;
		var_dump($object);
		print StringConstant::PE;
		if($halt){
			exit();
		}
	}
	
	public static function out($in,$var){
		$hr = "\n\n---------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
		var_dump($hr."*** $in :",$var,$hr);
	}
	
	/**
	 *    Identity test. Drops back to equality + types for PHP5
	 *    objects as the === operator counts as the
	 *    stronger reference constraint.
	 *    @param mixed $first    Test subject.
	 *    @param mixed $second   Comparison object.
	 *    @return boolean        True if identical.
	 *    @access public
	 */
	public static function isEqual($first, $second) {
		if (version_compare(phpversion(), '5') >= 0) {
			return self::isIdenticalType($first, $second);
		}
		if ($first != $second) {
			return false;
		}
		return ($first === $second);
	}
	
	
	/**
	 *    Recursive type test.
	 *    @param mixed $first    Test subject.
	 *    @param mixed $second   Comparison object.
	 *    @return boolean        True if same type.
	 *    @access private
	 */
	private static function isIdenticalType($first, $second) {
		if (gettype($first) != gettype($second)) {
			return false;
		}
		if (is_object($first) && is_object($second)) {
			if (get_class($first) != get_class($second)) {
				return false;
			}
			return self::isArrayOfIdenticalTypes(
					(array) $first,
					(array) $second);
		}
		if (is_array($first) && is_array($second)) {
			return self::isArrayOfIdenticalTypes($first, $second);
		}
		if ($first !== $second) {
			return false;
		}
		return true;
	}
	
	
	
	/**
	 *    Recursive type test for each element of an array.
	 *    @param mixed $first    Test subject.
	 *    @param mixed $second   Comparison object.
	 *    @return boolean        True if identical.
	 *    @access private
	 */
	private static function isArrayOfIdenticalTypes($first, $second) {
		if (array_keys($first) != array_keys($second)) {
			return false;
		}
		foreach (array_keys($first) as $key) {
			$is_identical = self::isIdenticalType(
					$first[$key],
					$second[$key]);
			if (! $is_identical) {
				return false;
			}
		}
		return true;
	}
	
	public static function hasEqualPropertyValues($obj1,$obj2,$strict=false){
		if(is_object($obj1)){
			if(is_object($obj2)){
				$obj1Properties = array();
				foreach($obj1 as $key=>$value){
					$obj1Properties[$key]=$value;
				}
				
				$obj2Properties = array();
				foreach($obj2 as $key=>$value){
					$obj2Properties[$key]=(array)$value;
					if(is_object($value)){
						if(!self::hasEqualPropertyValues($obj1Properties[$key],$value)){
							return false;
						}
					}else{
						if(!isset($obj1Properties[$key])){
							Logger::debug('no property exists for key <'.$key.'> to compare with',self::TAG);
							if($strict){
								return false;
							}else{
								break;
							}
						}
						if($value != $obj1Properties[$key]){
							
							Logger::debug('values do not match for key <'.$key.'> => '
									.print_r($value,true).' , original value = '
									.print_r($obj1Properties[$key],true),self::TAG);
							return false;
						}
					}
				}
				return true;
			}else{
				return false;
			}
		}else{
			if(is_object($obj2)){
				return false;
			}
			return $obj1===obj2;
		}
		
	}
	
}
?>
