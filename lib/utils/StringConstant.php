<?php namespace Foundation\Utils;
/**
 * Define convenience string contants here to use troughout the application.
 */

class StringConstant{

const PR = '<pre>';
const PE='</pre>';
const SCR_OPEN = "<script type=\"text/javascript\">\n\n";
const SCR_CLOSE = "</script>\n\n";
const EOL = "\n";
const TAB = "\t";
const EMP = '';
const CSV_SEPARATOR = ',';
const HR = '---------------------------------------------------------------------------------------------------------';
const SP = ' ';

}


?>