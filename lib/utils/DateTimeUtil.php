<?php
namespace Foundation\Utils;
use DateTimeZone;
use Foundation\Utils\Logger as Logger;
/**
 * class for DateTime helper functions
 * 
 * @author Aaldert van Weelden
 */
class DateTimeUtil{
	
	
	private static $instance;
	const TAG = 'DATETIMEUTIL';
	const DATETIMEZONE = 'Europe/Amsterdam';
	const DEFAULT_DATETIMEFORMAT = 'Y-m-d H:i:s';
	const DEFAULT_DATE_FORMAT = 'Y-m-d';
	const ISO8601_DATETIMEFORMAT = "Y-m-d'T'H:i:s";
	
	//allowed formats
	const EUROPEAN_DATE_FORMAT = 'd-m-Y';
	const EUROPEAN_DATETIME_FORMAT = 'd-m-Y H:i:s';
	const EUROPEAN_TIME_FORMAT = 'H:i:s';
	const EUROPEAN_DATETIME_NOSECONDS_FORMAT = 'd-m-Y H:i';
	
	
	public static function getInstance() {
		if(!isset(self::$instance)) {
			self::$instance = new DateTimeUtil();
		}
		return self::$instance;
	}
	
	private function __construct(){

	}
	
	/**
	 * Return the current  datetime string in MySQL datetimestamp format, corrected with the current timezone
	 * 
	 * @param string $dateTimeZone
	 * 			The dateTimeZone to get the current iso8601tz format timestamp for, eg: 'Europe/Amsterdam'
	 * @param string SdateTimeFormat
	 * 			The dateTimeFormat string, you can also use the preformatted constants, eg DEFAULT_DATETIMEFORMAT
	 * @return string The MySQL timestamp formatted string , corrected for the provided datetime zone
	 */
	public static function getNow($dateTimeFormat=self::DEFAULT_DATETIMEFORMAT){
		$time = time () +self::getTimeZoneOffset();
		$now=date($dateTimeFormat,$time);
		return $now;
	}
	
	/**
	 * Return the current GMT datetime string in MySQL datetimestamp format, 
	 * 
	 * @param string $dateTimeFormat
	 * 			The dateTimeFormat string
	 * @return string The MySQL GMT timestamp formatted string
	 */
	public static function getNowGMT($dateTimeFormat=self::DEFAULT_DATETIMEFORMAT){
		$now=date($dateTimeFormat);
		return $now;
	}
	
	/**
	 * Return the GMT datetime in ISO8601TZ format. Add the tz offset to retrieve the local datetime.
	 * @param string $dateTiemZone
	 * 			The dateTimeZone to get the current iso8601tz format timestamp for, eg: 'Europe/Amsterdam'
	 * @return the ISO8601TZ string , eg 2004-02-12T15:19:21+00:00
	 */
	public static function getNowISO8601tz($dateTimeZone=self::DATETIMEZONE){
		$now=date("c");
		return self::setTimezone($now,$dateTimeZone);
	}
	
	/**
	 * convert a timestamp to the  ISO8601TZ format
	 * @param string $timestamp
	 * 			The timestamp in mySQL format  eg: 2013-03-30 02:26:03
	 *  @param string $dateTiemZone
	 * 			The dateTimeZone to get the current iso8601tz format timestamp for, eg: 'Europe/Amsterdam'
	 * 
	 * @return string The iso8601 tz formatted timestamp, corrected for the provided dateTimeZone
	 */
	public static function getDateTimeISO8601tz($timestamp, $dateTimeZone=self::DATETIMEZONE){
		$time = strtotime($timestamp) + self::getTimeZone($timestamp,$dateTimeZone)*3600;
		$offset = self::getTimeZoneOffset($dateTimeZone);//offset in seconds
		$iso8601tz = date("c",$time - $offset);
		return self::setTimeZone($iso8601tz,$dateTimeZone);
	}
	
	
	/**
	 * convert a timestamp to the  MySql timestamp format
	 * @param string $timestamp
	 * 			The timestamp in    eg: 11-01-1963 or whatever can be parsed
	 *  @param string $dateTimeZone
	 * 			The dateTimeZone to get the current iso8601tz format timestamp for, eg: 'Europe/Amsterdam'
	 *
	 * @return string The MySql formatted timestamp, corrected for the provided dateTimeZone
	 */
	public static function toMySQLtimeStamp($timestamp, $dateTimeZone=self::DATETIMEZONE, $dateTimeFormat = self::DEFAULT_DATETIMEFORMAT){
		if(Util::nullOrEmpty($timestamp))  return null;
		return self::getMySQLtimeStamp(self::getDateTimeISO8601tz($timestamp, $dateTimeZone),$dateTimeZone,$dateTimeFormat);
	}
	
	
	public static function getMySQLtimeStamp($iso8601tz,  $dateTimeZone=self::DATETIMEZONE, $dateTimeFormat = self::DEFAULT_DATETIMEFORMAT){
		//TODO check if timezone offset is correctly parsed
		$offset = self::getTimeZone($iso8601tz, $dateTimeZone);
		$time = strtotime( substr($iso8601tz, 0, 19) );
		return date($dateTimeFormat,$time + $offset*3600);
	}
	
	/**
	 * Returns the timezone specified by the provided iso8601tz string in hours
	 * If no valid string is specified or no string at all the default configuration timezone is returned
	 * 
	 * @param string $iso8601tz
	 * 			The datetime string in iso8601tz format
	 * @param string $dateTimeZone
	 * 			The timezone location string used for calculating the timezone offset if no iso8601tz input string is provided
	 * 
	 * @return  integer The offset in hours
	 */
	public static function getTimeZone($iso8601tz=null,$dateTimeZone=self::DATETIMEZONE){
		if($iso8601tz==null){
			//Logger::debug('provided iso8601tz is <null>,returning default timezone offset',self::TAG);
			$timeOffset = self::getTimeZoneOffset($dateTimeZone);
			return $timeOffset/3600;
		}
		if(!is_string($iso8601tz) ){
			$error = 'provided iso8601tz['.$iso8601tz.'] is not a string, returning default timezone offset';
			//Logger::error($error,self::TAG);
			//throw new InvalidArgumentException($error);
			$timeOffset = self::getTimeZoneOffset($dateTimeZone);
			return $timeOffset/3600;
		}
		$hour = (integer)substr($iso8601tz, 19,3 );
		$minute = (integer)substr($iso8601tz, 23,2 );
		$offset = $hour + $minute/60;
		
		return (integer)round($offset);
		
	}
	
	/**
	 * returns the timeoffset in seconds for the provided DateTimeZone  string compared with GMT timezone.
	 * If none is provided the configured DateTimeZone is used, eg: 'Europe/Amsterdam'
	 * 
	 * @param String $dateTimeZone
	 * 			The dateTimeZone to lookup the offset for
	 * @return integer The offset in seconds
	 */
	public static function getTimeZoneOffset($dateTimeZone=self::DATETIMEZONE){
		if($dateTimeZone==null){
			$error = 'provided dateTimeZone is null';
			//Logger::error($error,self::TAG);
			//throw new InvalidArgumentException($error);
			return 0;
		}
		if(!is_string($dateTimeZone) ){
			$error = 'provided dateTimeZone is not a string';
			//Logger::error($error,self::TAG);
			//throw new InvalidArgumentException($error);
			return 0;
		}
		$offset = self::getOffset(trim($dateTimeZone));
		return (int) round($offset);
	}
	
	/**
	 * Returns the iso8601tz string with the correct timezone offset
	 * @param string  $iso8601tz
	 * 			The datetime in iso8601tz or iso8601 format
	 * @param string $dateTimeZone
	 * 		The dateTime zone eg: Europe/Amsterdam
	 */
	public static function setTimezone($iso8601tz,$dateTimeZone=self::DATETIMEZONE){
		if($iso8601tz==null){
			$error = 'provided iso8601tz is null';
			//Logger::error($error,self::TAG);
			throw new InvalidArgumentException($error);
		}
		if($dateTimeZone==null){
			$error = 'provided dateTimeZone is null';
			//Logger::error($error,self::TAG);
			throw new InvalidArgumentException($error);
		}
		$offset = self::getTimeZoneOffset($dateTimeZone);
		$tzBase = substr($iso8601tz, 0, 19);

		if($offset>=0){
			$tz = sprintf('+%1$02d:00', $offset/3600 );
		}else{
			$tz = sprintf('%1$03d:00', $offset/3600 );
		}
		return $tzBase.$tz;
		
	}
	
	/**
	 * Add houroffset to provided timestamp
	 * @param String $timestamp  in mySQL datetime format
	 * @param integer $hour
	 * @param String $dateTimeFormat The format string, DEFAULT_DATETIMEFORMAT
	 * @return The mySQL datetime with the hour added
	 * @throws InvalidArgumentException
	 * 
	 */
	public static function addToTimeStamp($timestamp, $hour, $dateTimeFormat=self::DEFAULT_DATETIMEFORMAT){
		if(Util::nullOrEmpty($timestamp)){
			$error = 'provided timestamp is null or empty';
			//Logger::error($error,self::TAG);
			throw new InvalidArgumentException($error);
		}
		if($hour==null){
			$error = 'provided hour to add is null';
			//Logger::error($error,self::TAG);
			throw new InvalidArgumentException($error);
		}
		$time = strtotime($timestamp);
		$offset = round( (int) $hour * 3600 );
		return date($dateTimeFormat,$time + $offset);
	}
	
	/**
	 * Return the transition object if available, use
	 * @param object  $timezone
	 */
	public static function timezone($timezone = self::DATETIMEZONE){
		$tz = new DateTimeZone($timezone);
		$transitions = $tz->getTransitions();
		if (is_array($transitions)){
			foreach ($transitions as $k => $t){
				// look for current year
				if (substr($t['time'],0,4) == date('Y')){
					$trans = $t;
					break;
				}
			}
		}
		return (isset($trans)) ? (object) $trans : false;
	}
	
	/**
	 * Return TRUE if the timezone is in Dailight Saving Time, FALSE if not.
	 * Returns null if the DST is not available for detecting (PHP version < 5.2)
	 */
	public static function isDST($dateTimezone = self::DATETIMEZONE){
		$tz = self::timezone($dateTimezone);
		if(is_object($tz)){
			return $tz->isdst;
		}
		return null;
	}
	
	/**
	 * Return the offset in seconds
	 * Returns null if the DST is not available for detecting (PHP version < 5.2)
	 */
	public static function getOffset($dateTimezone = self::DATETIMEZONE){
		$tz = self::timezone($dateTimezone);
		if(is_object($tz)){
			return $tz->offset;
		}
		return null;
	}
	
	/**
	 * Parsed the duration in millisecs to format h:m
	 * @param integer $duration
	 * @return the parsed format as string
	 */
	public /*string*/ static function parseDuration($duration){
		
		
		$seconds = floor($duration / 1000);
		$minutes = floor($seconds / 60);
		$hours = floor($minutes / 60);
		$milliseconds = $duration % 1000;
		$seconds = $seconds % 60;
		$minutes = $minutes % 60;
		
		//$format = '%u uur %02u min.';
		//$time = sprintf($format, $hours, $minutes);
		
		$format = '%u';
		$time = sprintf($format, $hours);
		return rtrim($time, '0');
		
	}
	
	/**
	 * Return the formatted datetime string
	 * @param string $timestamp
	 * @param The format
	 * @return the formatted datetime string
	 */
	public static function parseDate(/*String*/ $timestamp, $format){
		if(Util::nullOrEmpty($timestamp)) return null;
		$time = strtotime($timestamp);
		return date($format,$time);
		
	}
}