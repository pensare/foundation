<?php namespace Foundation\Utils;
/**
 * helperclass to provide structured string building tools
 */
class StringBuilder{
	
	protected $result;
	
	
	public function __construct($str=null){
		$this->result = '';
		if($str!=null){
			$this->result .= (string) $str;
		}
	}
	
	/**
	 * Append the input to the result string
	 * @param mixed $in
	 */
	public function append($in=StringConstant::EMP){
		
		$this->result .= (string) $in;
	}
	
	/**
	 * Append the input to the result string with an EOL added
	 * @param mixed $in
	 */
	public function appendEOL($in=StringConstant::EMP){
		$this->append($in);
		$this->result .= StringConstant::EOL;
	}
	
	/**
	 * Prepend the input to the result string
	 * @param mixed $in
	 */
	public function prepend($in=StringConstant::EMP){
	
		$this->result = (string) $in.$this->result;
	}
	
	/**
	 * Prepend the input to the result string with an EOL added
	 * @param mixed $in
	 */
	public function prependEOL($in=StringConstant::EMP){
	
		$this->result = (string) $in.StringConstant::EOL.$this->result;
	}
	
	/**
	 * Append the oject dump to the string
	 * @param object $obj
	 */
	public function appendObject($obj=null){
		
		$this->result .= StringConstant::HR;
		$this->result .= print_r($obj,true);
		$this->result .= StringConstant::HR;
	}
	
	/**
	 * Return or output the string 
	 * @param boolean $prnt If TRUE, then print the string
	 * @param boolean $nl2br If TRUE, the output is converted: \n -> &lt;br&gt;
	 * @return Ambigous <string, mixed> uotput
	 */
	public function toString($prnt=false, $nl2br = false){
		if($prnt){
			print $this->result;
		}else{
			return $this->result;
		}
		
	}
	
	public function isEmpty(){
		
		return empty($this->result);
	}
	
}
?>