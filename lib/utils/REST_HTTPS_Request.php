<?php
namespace Foundation\Utils;


/**
 * Class encapsulating the RESTfull call result
 * 
 * @author Aaldert van Weelden
 *
 */

class REST_HTTPS_Response{
	
	const ERROR = "ERROR";
	const OK = "OK";

	public $payload;
	public $status;
	public $info;
	public $statusCode;

	public function __construct($payload, $info){
		if($payload===null || $payload===false || empty($payload)){
			$this->payload = null;
			$this->status = self::ERROR;
				
		}else{
			$this->payload = $payload;
			$this->status = self::OK;
			
		}
		$this->setInfo($info);
	
	}
	
	private function setInfo($info){
		if($info == null || empty($info)){
			$this->info = null;
			$this->statusCode = 0;
		}else{
			$this->info = (object) $info;
			$this->statusCode = $this->info->http_code;
		}
	}


}


/**
 * Class for facilitating the call to a RESTful service HTTPS endpoint
 * 
 * For now only GET will do
 	*
 * @author Aaldert van Weelden
 *
 */
class REST_HTTPS_Request{
	
	
	/**
	 * Send a GET request to the provided endpoint.
	 * @param string $endpoint
	 * @param array $parameters  The associative array of request parameters, can be NULL
	 * @return \Utils\REST_HTTPS_Response  $response
	 * 						The response object
	 */
	public static  function doGET($endpoint, $parameters=null){
		
		if($parameters !==null &&  is_array($parameters) && !empty($parameters)){
			$url = $endpoint.'?'.http_build_query($parameters);
		}else{
			$url = $endpoint;
		}
	
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		$response = new \Utils\REST_HTTPS_Response(curl_exec($ch), curl_getinfo($ch));
		
		curl_close($ch);
	
		return $response;
	}
	

}


?>