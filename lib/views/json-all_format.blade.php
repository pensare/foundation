<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="JSON prettifier">
    <meta name="author" content="Aaldert van Weelden">
 

    <title>JSON output</title>
    
    <style type="text/css">
    	pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; font-family: consolas; font-size: 12px;}
		.string { color: green; }
		.number { color: darkorange; }
		.boolean { color: blue; }
		.null { color: magenta; }
		.key { color: black; }
    </style>
    
  </head>

  <body>

   
  </body>
  
   <script type="text/javascript">
    
	    function output(inp) {
	        document.body.appendChild(document.createElement('pre')).innerHTML = inp;
	    }
	
	    function syntaxHighlight(json) {
	        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
	            var cls = 'number';
	            if (/^"/.test(match)) {
	                if (/:$/.test(match)) {
	                    cls = 'key';
	                } else {
	                    cls = 'string';
	                }
	            } else if (/true|false/.test(match)) {
	                cls = 'boolean';
	            } else if (/null/.test(match)) {
	                cls = 'null';
	            }
	            return '<span class="' + cls + '">' + match + '</span>';
	        });
	    }
	
	    var obj = {{$data}};
	    var str = JSON.stringify(obj, undefined, 4);
	    
	    output(syntaxHighlight(str));

    </script>
    
    
</html>
