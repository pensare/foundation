<?php namespace Foundation;
use Foundation\Utils\StringBuilder;
use Foundation\Utils\StringConstant;

/**
 * Class used to generate a nice formatted report
 */

class CommandReport extends StringBuilder{
	
	const INFO  = 'Info';
	const WARN  = 'Warn';
	const ERROR = 'Error';
	
	public $level;
	
	public function __construct($level = self::INFO){
		$this->level = $level;
	}
	
	/**
	 * Add header and footer to the message body
	 *
	 * @param string $header
	 * @param string $footer
	 * @param boolean $nl2br
	 * @return String result
	 */
	public function format($header = null, $footer = null, $nl2br = false){
		if($this->isEmpty()) return StringConstant::EMP;
		
		if($header===null) $header = StringConstant::EOL.'*** start of report ***';
		if($footer===null) $footer = '*** end of report ***';
		$this->prependEOL('Level: '.$this->level);
		$this->prependEOL($header);
		$this->appendEOL($footer);
		if($nl2br){
			$this->toString(false, true);
		}
		return $this->toString();
	}
	
	
}

?>