<?php namespace Foundation;
use  Foundation\Utils\Util as Util;
/**
 * Class used to transmit the data in te callback between {abstract}Commander execute() 
 * and {abstract}Commands onExecuted() implementations
 * 
 * @see Commander
 * @see Commands
 */
class CommandCallbackResult{
	
	
	const SUCCESS = true;
	const FAILURE = false;
	
	private /*String*/ $id;
	private /*String*/ $name;
	private /*String*/ $description;
	private /*integer*/ $code;
	private /*string*/ $sender;
	
	private /*Boolean*/ $success;
	private /*XhrError*/ $error;
	private /*String*/ $message;
	
	private /*Object*/ $data;
	
	/**
	 * Constructor
	 *
	 * @param String $name
	 * @param String $description
	 * @param String $success
	 */
	public function __construct($name=null,$description=null,$success=self::SUCCESS){
		
		$this->setName($name);
		$this->setDescription($description);
		$this->setSuccess($success);
		$this->id = Util::create_GUID();
		$this->data = new \StdClass;
	}
	
	public function getID(){
		return $this->id;
	}
	
	public function setID($id){
		$this->id=$id;
	}
	
	public function getData(){
		return $this->data;
	}
	
	public function setData($data){
		$this->data=$data;
	}
	
	public function isSuccess(){
		return $this->success;
	}
	
	public function setSuccess($success){
		$this->success=$success;
	}
	
	public function getCode(){
		return $this->code;
	}
	
	public function setSender($sender){
		$this->sender=$sender;
	}
	
	public function getSender(){
		return $this->sender;
	}
	
	public function setCode($code){
		$this->code=$code;
	}
	
	public function getMessage(){
		return $this->message;
	}
	
	public function setMessage($message){
		$this->message=$message;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function setName($name){
		$this->name=$name;
	}
	
	public function getDescription(){
		return $this->description;
	}
	
	public function setDescription($description){
		$this->description=$description;
	}
	
	/**
	 * Get the error
	 */
	public function getError(){
		return $this->error;
	}
	
	/**
	 * Set the error
	 * @param String $error
	 */
	public function setError($error){
		$this->error=$error;
	}
	
	/**
	 * Retrieves the merge of object en parent, used for cascading
	 * object to array conversion
	 */
	public function toArray() {
		return array_merge(get_object_vars($this));
	
	}
	
	
}

?>