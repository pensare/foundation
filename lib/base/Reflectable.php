<?php namespace Foundation;
/**
 * Class used to enable dynamic hydration
 * @author Aaldert van Weelden
 *
 */
abstract class  Reflectable{

	/**
	 * private, protected, public and static
	 * @var string ALL
	 */
	const ALL   		= 'all';
	/**
	 * protected and public
	 * @var string DATA
	 */
	const DATA			= ' data';
	/**
	 * private and protected
	 * @var string ENCAPSULATED
	 */
	const ENCAPSULATED  = 'encapsulated';
	/**
	 * private properties
	 * @var string LOCAL
	 */
	const LOCAL   		= 'private';
	/**
	 * protected properties
	 * @var string INHERITED
	 */
	const INHERITED   	= 'protected';
	/**
	 * 
	 * @var string EXPOSED
	 */
	const EXPOSED 		= 'public';
	/**
	 *
	 * @var string INVARIANT
	 */
	const INVARIANT 		= 'static';
       
    /**
     * Method used for the retrieval of the entity properties
     * @return  The array with the properties of this object
     */
    public function getProperties($scope=self::EXPOSED){
    	switch($scope){
    		case self::ALL:
    			$show = \ReflectionProperty::IS_PRIVATE | \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_STATIC;
    			break;
    		case self::DATA:
    			$show = \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PUBLIC;
    			break;
    		case self::ENCAPSULATED:
    			$show = \ReflectionProperty::IS_PRIVATE | \ReflectionProperty::IS_PROTECTED;
    			break;
    		case self::LOCAL:
    			$show = \ReflectionProperty::IS_PRIVATE;
    			break;
    		case self::INHERITED:
    			$show = \ReflectionProperty::IS_PROTECTED;
    			break;
    		case self::EXPOSED:
    			$show = \ReflectionProperty::IS_PUBLIC;
    			break;
    		case self::INVARIANT:
    			$show = \ReflectionProperty::IS_STATIC;
    			break;
    		default:
    			throw new \InvalidArgumentException('No valid Refelctabel scope provided');
    			break;
    					
    	}
    	
    	$properties = array();
    	$pairs = array();
    	$reflect = new ReflectionClass($this);
    	$props = (array) $reflect->getProperties($show);
    	 
    	foreach($props as $prop){
    		$properties[]=$prop->name;
    	}
    	return $properties;
    	
    }
    
    /**
     * Find and return the content with the requested key in the provided property.
     * Example: $this->dto->findIn('bijlagedata.bestandsnaam', 'OSO-dossier.xml')->bestand
     * @param string $key  The key, can be dot separated
     * @param string $value  The value to search for
     * @return object The found object or NULL
     */
    public abstract function findIn($key, $value);
    
    /**
     * Updates the fields of the object, source is a plain standard object
     * @param $request  The plain request object 
     */
    public abstract function requestToDTO($request); 

    /**
     * @param object $obj  The object to test for
     * @return boolean Returns TRUE if the provided object equals this dto
     */
    public /*boolean*/ abstract function equals($obj);
    
    /**
     * Convert the content of the dto to an array
     * @return array DTO as array
     */
    public /*array*/  abstract function toArray();
    
}              

?>