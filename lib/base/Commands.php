<?php namespace Foundation;
interface Commands{
	
	/**
	 * Returning the result of a command from the executed command in the callback on success
	 * 
	 * @param Object $result
	 */
	public function onExecuted($result);
}
?>