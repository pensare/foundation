<?php namespace Foundation;
/**
* Class to implement enumerations for PHP 5 (without SplEnum) 
* 
* @link http://github.com/marc-mabe/php-enum for the canonical source repository
* @copyright Copyright (c) 2012 Marc Bennewitz
* @license http://github.com/marc-mabe/php-enum/blob/master/LICENSE.txt New BSD License
*/

abstract class Enum
{
    /**
     * The current selected value, must be public for json serialization
     * @var mixed
     */
    public $value = null;
    
    /**
     * An array of available constants, must be public for json serialization
     * @var array
     */
    public $constants = null;

    /**
     * Constructor
     * 
     * @param mixed $value The value to select
     * @throws InvalidArgumentException
     */
    final public function __construct($value = null){

        $reflectionClass = new ReflectionClass($this);
        $this->constants = json_decode(json_encode($reflectionClass->getConstants()), true);
       
        if ($value!=null) {
        	if(! is_integer($value)){
        		throw new InvalidArgumentException('ENUM: provided value in constructor must be an integer');
        	}
            $this->setValue($value);
        } elseif (!in_array($this->value, (array)$this->constants, true)) {
            throw new InvalidArgumentException("ENUM: No value given in constructor and no default value defined");
        }
    }

    /**
     * Get all available constants
     * @return array
     */
    final public function getConstants()
    {
        return $this->constants;
    }
    
    /**
     * Select a new value
     * @param mixed $value
     * @throws InvalidArgumentException
     */
    final public function setValue($value){
    	
    	if(! is_integer($value)){
    		throw new InvalidArgumentException('ENUM: provided value in setValue() must be an integer');
    	}
        if (!in_array($value, (array)$this->constants, true)) {
            throw new InvalidArgumentException("ENUM: Unknown value in setValue() '{$value}'");
        }
        $this->value = $value;
    }
    
    /**
     * Select a new value
     * @param mixed $value
     * @throws InvalidArgumentException
     */
    final public function set($value){
    	
    	if(!is_integer($value) ){
    		throw new InvalidArgumentException('ENUM: provided value in set() must be an integer');
    	}
    	if (!in_array($value, (array)$this->constants, true)) {
    		throw new InvalidArgumentException("ENUM: Unknown value in set() '{$value}'");
    	}
    	$this->value = $value;
    }

    /**
     * Get the current selected value
     * @return mixed
     */
    final public function getValue(){
        return $this->value;
    }

    /**
     * Select a new value by constant name
     * @param string $name
     * @throws InvalidArgumentException
     */
    final public function setName($name){
        if (!array_key_exists($name, (array)$this->constants)) {
            throw new InvalidArgumentException("ENUM: Unknown name  in setName() '{$name}'");
        }
        $this->value = $this->constants[$name];
    }

    /**
     * Get the current selected constant name
     * @return string
     */
    final public function getName(){
        return array_search($this->value, (array)$this->constants, true);
    }

    /**
     * Get the current selected constant name
     * @return string
     * @see getName()
     */
    final public function __toString(){
        return $this->getName();
    }
    
    /**
     * Get the current selected value
     * @return mixed
     * @see getValue()
     */
    final public function __invoke(){
        return $this->getValue();
    }
    
    
}