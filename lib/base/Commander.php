<?php namespace Foundation;
/**
 * Class used as baseclass for the commanders. 
 * Commands should extend from this class, don't instantiate directly
 * 
 * @author Aaldert van Weelden
 */

abstract class Commander{
	
	
	/**
	 * Call this function to execute a predefined command,
	 * @param string $command  The  value of the command name enumaration
	 * @param array $callback The returned result as an array
	 * @param mixed $data
	 */
	public abstract function execute($command,$callback, $data);
	
}