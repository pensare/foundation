# Pensare foundation #

A Pensare php and JS foundation library 

## What is this library for? ##

### PHP classes ###

* helper classes
* logger classes
* base classes
* datetime utils
* sql utils
* command classes

### Javascript classes ###

Use copy2assets util  to transfer JS classes form vendor library to the application's public assets directory

Usage:  php copy2assets.php [assetpath\relative\to\applicationroot]  eg  public\assets for laravel

### Updating your project: ###

Remember to merge  stable versions only to the Foundation master branch, or else projects in production elsewhere which have a dependecy on the foundation could go down in flames

* After commiting your changes to the foundation, change directory to your project root directory
* Lookup the VWIT commit hash in your project composer.lock file
* Replace the hash wit the commit you want to load into your project /vendor directory
* run composer install
* run php vendor/pensare/foundation/copy2assets.php  public\assets  in order to transer the asset JS classes OR:
* add the following section to your application root composer.json :

		"scripts": {
			"post-install-cmd": [
				"php vendor/pensare/foundation/copy2assets.php public/assets"
			]
		}

Private licence is applied