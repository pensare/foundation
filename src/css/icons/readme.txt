Staging is a �production-like� environnement : asset-pipeline with digest on my rails application, assets served using a CDN�
And here is the problem : Firefox doesn�t now allow cross domain fonts.
Since I host the assets on my CDN (just another virtual host to serve static file), i modified the Apache configuration with this workaround (thanks Catalin Rosu) :

<FilesMatch "\.(ttf|otf|eot|woff)$">
  <IfModule mod_headers.c>
    Header set Access-Control-Allow-Origin "*"
  </IfModule>
</FilesMatch>

This is for Apache server but can be adapted for Nginx.
Note that you have to load mod_headers.c (apache headers module)if not already done :

sudo ln -s /etc/apache2/mods-available/headers.load /etc/apache2/mods-enabled/
Restart Apache and everything should work.