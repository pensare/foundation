/*********************
 * dependencies : util.js for cookie handling and location handling
 *
 *
 *
 *
*/

//GLOBALS
//to do : use cookie from util.js
var GL = {
	string		: '',
	focus 		: false,
	hash		: ''
}
createCookie('script','true');//javascriptdetectie

//===========hash functies=======//


function getHash(forceload,sender){//forceload=true : laadt hash in ST{postvars} en laadt resultaten
	//checkt periodiek of de url-hash wordt gewijzigd
	!sender?sender='load':void(0);
	if(getSender()){
		sender=getSender();
	}
	var interval=400;//to do : make global
	var value=window.location.hash;
	//caching
	if(value != '' && typeof(value)!='undefined'){
		if(readCookie('hash')){
			eraseCookie('hash');
		}
		createCookie('hash',value);//cache
	}
	if( (value != GL.hash ) || forceload){
		goBackTo(page,sender);
	}
		
	GL.hash=value;

	setTimeout("getHash()", interval);
	
}
window.onload=function(){
	getHash();
	setTimeout("clearSender()",2000);
}

function setHash(hash){
	window.location.hash=hash;
}

function clearHash(){
	window.location.hash='';
	if(readCookie('hash')){
				eraseCookie('hash');
			}
	clearGlobs();
	getHash(true,btn_wisall);
}


//cookie
function goBackTo(page,sender){
	
	var value=readCookie('hash'),
		root=_getRoot();
	if(!value){
		value='';
	}
	setSender(sender);
	window.location.replace(root+value);
	
}

function setSender(sender){
	if(readCookie('sender')){
		eraseCookie('sender');
	}
	createCookie('sender',sender);
	return true;
}
function getSender(){
	return readCookie('sender');
}
function clearSender(){
	eraseCookie('sender');
}

//eventueel uit util.js
function _getURLbase(){
	var root,ret;
	root=document.URL.split('//')[1];
    ret=root[0]+root[1].split('/')[0]+'/';
	return ret;
};
//util.js
function _getRoot(){
	var d=document.location,
		aUrl={};
			
	aUrl=d.pathname.split('/');
	aUrl.pop();
	
	d.root=d.protocol+'//'+d.host+aUrl.join('/')+'/';
	return d;
			
};