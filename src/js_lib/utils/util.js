/*
PUBLIC METHODS :

onDocumentReady	:
   * @description attaches an eventhandler to the document onload event. fires when DOM is ready ,
   * @method onDocumentReady
   * @public
   * @static
   * @param {function}  eventhandler
   * @return void

addListener		:
   * @description generic eventhandler attacher ,
   * @method addListener
   * @public
   * @static
   * @param {object}  elem : element to which event is attached
   * @param {string}  evtType : event type without prefix 'on'
   * @param {function} callback : callback function
   * @return void    
        
    	
removeListener	: function(elem, evtType, callback){
   * @description generic eventhandler detacher ,
   * @method removeListener
   * @public
   * @static
   * @param {object}  elem : element to which event is to be detached
   * @param {string}  evtType : event type without prefix 'on'
   * @param {function}  callback : callback function
   * @return void      	
      
getEvent		:  
   * @description generic window event getter ,
   * @method getEvent
   * @public
   * @static
   * @param {object}  event : event 
   * @return {object} event    
    	
    	   	
getTarget		:  
   * @description generic window event target getter ,
   * @method getTarget
   * @public
   * @static
   * @param {object}  event : the event 
   * @return {object} target : the event's target    

getVal 		: 
   * @description gets value of a form element ,
   * @method getVal
   * @public
   * @static
   * @param {string || object}  form element, name or id
   * @return {string} value  
setVal 		: 
   * @description sets value of a form element ,
   * @method setVal
   * @public
   * @static
   * @param {string || object}  form element, name or id
   * @param {string}  value
		
setHTML		:
   * @description sets html in specified element ,
   * @method setHTML
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name
   * @return void   
		
getHTML		:
   * @description gets html content of specified element ,
   * @method getHTML
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name
   * @return void   
		
getRef 		:
   * @description returns an object with reference to an element ,
   * @method getRef
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name
   * @return void   

setStyle	:
   * @description sets the css-style properties of specified element ,
   * @method setStyle
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name
   * @param {string}  css property
   * @return {string} css value 
					  
getStyle	:
   * @description returns the css-style propertie of specified element ,
   * @method getStyle
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name
   * @param {string}  css property
   * @param {string}  css value
   * @return void  
		
doShow		: 
   * @description sets the display or visibility property of a specified element to visible,
   * @method doShow
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name
   * @param {int}  time, if specified the displayed element will disappear after the time-value im ms
   * @return void
   
doHide		:
   * @description sets the display or visibility property of a specified element to hide,
   * @method doHide
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name
   * @return void
doShowHide	:
   * @description toggles the display or visibility property of a specified element ,
   * @method doShowHide
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name
   * @return void
					  
addClassName	: 
   * @description adds a specified css-class to an element ,
   * @method addClass
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name
   * @param {string}  classname, specified in HTML-document or linked css-document
   * @return void
   
addStylesheet	: 
   * @description adds a specified css-stylesheet to document ,
   * @method addStylesheet
   * @public
   * @static
   * @param {string}  stylesheet url
   * @return void
					  
setFocus 	:
   * @description sets focus to a specified element ,
   * @method setFocus
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name
   * @return void

URIencode 	:
   * @description URIencodes [utf-8] a string  with parameters
   * @method URIencode
   * @public
   * @static
   * @param {string}  URI string
   * @return {string} encoded string

createUUID  :
   * @description creates a random UUID
   * @method createUUID
   * @public
   * @static
   * @return the generated UUID
   * 
selectText 	:
   * @description selects a range of text in input-element
   * @method selectText
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name of textbox
   * @param {int}  startIndex
   * @param {int}  stopIndex
   * @return void
   
posX 	:
   * @description calculates left coordinates dom-element
   * @method posX
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name of element
   * @return {int}  left position
   
posY 	:
   * @description calculates top coordinates dom-element
   * @method posY
   * @public
   * @static
   * @param {string || object}  document element , id, tag, name of element
   * @return {int}  top position

URIencode 	: 
   * @description URIencodes [utf-8] an escaped string  with parameters
   * @method URIencode
   * @public
   * @static
   * @param {string} decoded string
   * @return {string} URI string
URIdecode 	: 
   * @description URIdecodes [utf-8] an unescaped string
   * @method URIdecode
   * @public
   * @static
   * @param {string}  URI string
   * @return {string} decoded string
encode_utf8 	: 
   * @description encodes [utf-8] a string
   * @method encode_utf8
   * @public
   * @static
   * @param {string}  string
   * @return {string} encoded string
   
decode_utf8 	: 
   * @description decodes [utf-8] a string
   * @method decode_utf8
   * @public
   * @static
   * @param {string}  string
   * @return {string} decoded string
   
base64encode 	: 
   * @description base64encodes  a string
   * @method base64encode
   * @public
   * @static
   * @param {string}  string
   * @return {string} encoded string  
   
base64decode 	: 
   * @description base64decodes  a string
   * @method base64decode
   * @public
   * @static
   * @param {string}  string
   * @return {string} decoded string 

safeHTML		:
   * @description encodes html for posting as url
   * @method safeHTML
   * @public
   * @static
   * @param {string}  HTML
   * @return void
   
safeURI 		: 
   * @description encodes uri-component to base64 decoded JSON-string for sending as url
   * @method safeURI
   * @public
   * @static
   * @param {string}  uri
   * @return {string} decoded string
   
safeObjToURI 		: 
   * @description encodes JSON object to base64 decoded JSON-string for sending as url
   * @method safeObjToURI
   * @public
   * @static
   * @param {string}  obj
   * @return {string} decoded string
   ==========================================
   USE PHP FUNCTION :
   function uri2json($data,$b_JSON=false){
		$data=base64_decode($data);
		$data=urldecode($data);
		if(!$b_JSON){
			return $data;
		}
		else {
			return json_decode($data);
		}
	}
   =========================================
isArray		:
   * @description returns true if typeof object is array
   * @method isArray
   * @public
   * @static
   * @param {obj}  array
   * @return {boolean} true or false

isNumber		:
   * @description returns true if typeof object is number
   * @method isNumber
   * @public
   * @static
   * @param {obj}  number
   * @return {boolean} true or false
   
inStr		:
   * @description returns true if needle is in haystack
   * @method inStr
   * @public
   * @static
   * @param {string} needle
   * @param {string} haystack   
   * @return {boolean} true or false

getExacType		:
   * @description returns  type of parameter
   * @method getExactType
   * @public
   * @static
   * @param {obj}  inputobject
   * @return {string} type
   
removeHTMLTags	:
   * @description removes all html tags from parameter
   * @method removeHTMLTags
   * @public
   * @static
   * @param {string}  html
   * @return {string} text, no tags
   
intOnly 	:
   * @description returns true if argument is integer
   * @method intOnly
   * @public
   * @static
   * @param {string}  testvalue from field
   * @return {boolean} true or false
   
goTo 	:
   * @description redirection to url
   * @method goTo
   * @public
   * @static
   * @param {string}  url  , if only filename (no slashes)then root is added
   * @param {string}  get  , if specified get is added as parameter {root}{url}?d={get}
   * @param {string}  d    , if true get is added as uri component
   * @return void

objToKey 	:
   * @description returns array with objectkeys
   * @method objToKey
   * @public
   * @static
   * @param {object}  object
   * @return {array} array with keys

winOpen :
   * @description creates new browserwindow
   * @method winOpen
   * @public
   * @static
   * @param {object}    root        : {string}, [if not specified then root is determined from server-info]
						url         : {string},
						name        : 'win_'+Math.floor(Math.random()*10000),[random name]
						options     : 'menubar=1,resizable=1,scrollbars=1,width=600,height=850'
   * @return {object}	window
   

getLocation :
   * @description returns a extended location object
   * @method getLocation
   * @public
   * @static
   * @param {string}  href
   * @param {string}  param
   * @return {obj}  locationobject :		
				
	d.protocol
	d.host
	d.hostname
	d.pathname
	d.port
	d.reload
	d.place
	d.assign
	d.hash
	d.search
	//base object
	d.root
    d.base
	d.hashobject
	d.searchobject
   *
			
objToKey : 
   * @description returns an array with the object keys
   * @method objToKey
   * @public
   * @static
   * @param {object}  object to be converted
   * @return {array}  array with numeric keys, values are object keys
		
objFlip :
   * @description returns an object with keys and values switched
   * @method objFlip
   * @public
   * @static
   * @param {object}   object to be converted
   * @return {object}  object with oldvalues=>oldkeys
		
objToURL :
   * @description returns a uri-component string
   * @method objToURL
   * @public
   * @static
   * @param {object}   object to be converted to URI
   * @return {string}  URI string

URLtoObj :
   * @description returns an object derived from an URI, seperators are ? and #
   * @method URLtoObj
   * @public
   * @static
   * @param {string}   URI to be converted
   * @param {string}   seperator 1
   * @param {string}   separator 2
   * @return {object}  object name=>value pairs 

//see also Dough Crockford's JSON page www.json.org  
JSONparse :
   * @description parses a JSON string into an object
   * @method JSONparse
   * @public
   * @static
   * @param {string}   string to be converted
   * @param {boolean}  revive
   * @return {object}  object		
		
JSONtoString :
   * @description converst a JSON object into a JSON string
   * @method JSONtoString
   * @public
   * @static
   * @param {object}   object to be converted
   * @return {string}  JSON string		
		
		
/*		
 * cookies, no need for explanation i guess ;-)
 
 * setCookie : function(name, value, expires, path, domain, secure)

 * getCookie : function(name)
		
 * unsetCookie : function(name, path, domain, secure)
		
/* subcookies utilities
		
 * setSubcookie : function(name, subName, value, expires, path, domain, secure)
		
 * setallSubcookie : function(name, subcookies, expires, path, domain, secure)
	
 * getSubcookie : function(name, subName)
		
 * getallSubcookie : function(name)
		
 * unsetSubcookie : function(name, subName, path, domain, secure)
		
 * unsetallSubcookie : function(name, path, domain, secure)

getGlobals	:
   * @description    maps global interface object to local variabeles  ,
   * @method getGlobals
   * @public
   * @static
   * @param {object} | object with variabele names to map
   *                 | ['local var01', 'local var02', '>>' , 'local var03'] {array}
   *                 | '>>' skips one global var for mapping
   * @return {object} mapped local object
   
numberToCurrency 		: 
   * @description format string to currency ,
   * @method numberToCurrency
   * @public
   * @static
   * @param {number}  number
   * @return {string}  formatted string	

mailTo 		: 
   * @description javascript mailto link  ,
   * @method mailTo
   * @public
   * @static
   * @param {object}  	object with maildata
						{ 	"email":{emailadress},
							"subject":{subject},
							"body":{message text}
						}
   * @return {void}	   
   
objXHR 		: 
   * @description returns a crossbrowser XML HTTP Request object,
   * @method objXHR
   * @public
   * @static
   * @return {object}  XML HTTP Request object		

XHRsubmit 		: 
   * @description straightforward XML HTTP Request connection,
   * @method XHRsubmit
   * @public
   * @static
   * @param {string}	url
   * @param {string}	data  post data UIR component
   * @param {string}	mode  post or get
   * @return {object}  XML HTTP Request Result object	
    
getscriptUriParam	:
   *@description retrieve the uri parameters provided by the script tag url
   *@method getscriptUriParam
   *@param {string} paramKey  the key to look for
   *@return {string}  The key value, or <null> if none found

  //loggerfuncties---------------------------------------
		
assert :
   * @description sets loggermessage according to passed condition
   * @method assert
   * @public
   * @static
   * @param {object}  condition,  usually a function to evaluate
   * @return {string} message to be displayed
		
display :
   * @description displays message to loggerwindow  example : Util.display('form.js saveSuccessResult regel 105', oResponse, false, 2);
   * loggerwindow can be closed onclick
   * @method log
   * @public
   * @static
   * @param {string}  sender, format document, function, linenumber (eg : 'form.js saveSuccessResult regel 105')
   * @param {string}  item to be displayed
   * @param {boolean} obligated : add,  if argument = true then items will be added to loggerwindow content
   * @param {object} collapse : an object with child object names that should be displayed collapsed initially. 
   * 				 Use this to prevent large initial dumps when there are large childs involved
   * @return void

logInit :
   * @description starts logging , generates loggerwindow
   * @method logInit
   * @public
   * @static
   * @param {boolean} || {string} if true then position = relative, loggerwindow is added after normal content. if false or omitted then loggerwindow is draggable,
								  if 'console' then logger output to firebug console,  if 'popup' then logger output to ajax popup window.
   * @return void

*/

//object functies------------------------------------------------
/*
extend :
   * @description extends an object with the properties and methods of another object
   * @method extend
   * @public
   * @static
   * @param {obj}  input
   * @param {obj}  extended object
   * @return {obj} extended object
   
inspect :
   * @description inspect an object by looping  inspect : function(obj, maxLevels, level)
   * @method inspect
   * @public
   * @static
   * @param {obj}  object to be inspected
   * @param {int}  maxLevels, depth of iteration
   * @param {int}  level  startlevel
   * @return {string} html with objectstructure as unordered list  
   
inheritprototype :
   * @description attaches an objects protoype to another, restoring the constructor, useful for constuctor stealing inherit pattern
   * @method inheritprototype
   * @public
   * @static
   * @param {obj}  subtype object
   * @param {obj}  supertype object
   * @return {void} 
   
length :
   * @description returns length of object [number of properties and methods]
   * @method length
   * @public
   * @static
   * @param {obj}  object
   * @return {number}  objectlength
   
   
   
*/

var Util = (function(){
	//PRIVATE STATIC CONSTANTS
	var ROOT     /*: string*/ = '';
	var UTIL;
	//global javascript variable APP_URL
	
	if(typeof(ASSET_URL)!=='undefined'){
		UTIL= ASSET_URL+'/js_lib/utils/';
	}else{
		if(typeof(console)!=='undefined'){
			console.warn('ASSET_URL is undefined,include local or global serverside vars. PAth ./public/assets/js_lib/utils is used as default');
		}
		UTIL ='./public/assets/js_lib/utils/';//to do; vanuit loggerOptions
	}
	
		
	var CNT_DEBUGGER         /*: string*/ = 'cnt_debugger_message',
		CNT_DISPLAY_MESSAGE  /*: string*/ = 'cnt_display_message',
		CNT_DEBUGGER_TXT     /*: string*/ = 'cnt_debugger_txt',
		IMG_DEBUGGER_CLEAR   /*: string*/ = ROOT+UTIL+'img/clear.gif',
		IMG_DEBUGGER_CLOSE   /*: string*/ = ROOT+UTIL+'img/close.gif',
		IMG_AJAXLOADER       /*: string*/ = ROOT+UTIL+'img/ajax-loader.gif',
		LOGGERSTYLE_URL      /*: string*/ = ROOT+UTIL+'logger/logger.css',
		ALERTSTYLE_URL      /*: string*/ = ROOT+UTIL+'logger/alert.css';
	
	//PRIVATE STATIC VARS
	var _is_debugger  /*: boolean*/ = false,//true als debugger is geinitialiseerd
		_is_onscreen  /*: boolean*/ = true,//true als debugger output naar screen
		_is_popup     /*: boolean*/ = false,//true als debugger output naar popup
	    _is_remote     /*: boolean*/ = false;//true als debugger output naar string
	
	//PRIVATE STATIC METHODS
	
	
	var _utf8 = {
	
	
	
		encode : function(sPlaintext){
			
			var plaintext = sPlaintext,
				SAFECHARS = "0123456789" +					// Numeric
							"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +	// Alphabetic
							"abcdefghijklmnopqrstuvwxyz" +
							"-_.!~*'()",					// RFC2396 Mark characters
				HEX = "0123456789ABCDEF",
				encoded = "";
				
			for (var i = 0; i < plaintext.length; i++ ) {
				var ch = plaintext.charAt(i);
				if (ch == " ") {
					encoded += "+";				// x-www-urlencoded, rather than %20
				} 
				else if (SAFECHARS.indexOf(ch) != -1) {
					encoded += ch;
				} 
				else {
					var charCode = ch.charCodeAt(0);
					if (charCode > 255) {
                        /*
						alert( "Unicode Character '" 
								+ ch 
								+ "' cannot be encoded using standard URL encoding.\n" +
								  "(URL encoding only supports 8-bit characters.)\n" +
								  "A space (+) will be substituted." );
                        */
                        
						//encoded += "+";
                        encoded += '|&#'+charCode+';|';
					} 
					else {
						encoded += "%";
						encoded += HEX.charAt((charCode >> 4) & 0xF);
						encoded += HEX.charAt(charCode & 0xF);
					}
				}
			} // for
			return encoded;
		},
	 
		decode : function(sEncoded){
		   
		   var 	encoded = sEncoded,
				HEXCHARS = "0123456789ABCDEFabcdef",
				plaintext = "";
				
		   var i = 0;
		   while (i < encoded.length) {
			   var ch = encoded.charAt(i);
			   if (ch == "+") {
				   plaintext += " ";
				   i++;
			   } 
			   else if (ch == "%") {
					if (i < (encoded.length-2) 
							&& HEXCHARS.indexOf(encoded.charAt(i+1)) != -1 
							&& HEXCHARS.indexOf(encoded.charAt(i+2)) != -1 ) {
						plaintext += unescape( encoded.substr(i,3) );
						i += 3;
					} 
					else {
						//alert( 'Foute escape combinatie bij ...' + encoded.substr(i) );
						plaintext+="%[ERROR]";
						i++;
					}
				} 
				else {
				   plaintext += ch;
				   i++;
				}
			} // while
            
            function convert(value){
                if(_inStr('|&#',value) ){
                    var result='',arr,sub,one,two,code;
                    arr = value.split('|&');
                    for(key in arr){
                    
                        sub=arr[key].split(';|');
                        sub[0]?one=sub[0]:one='';
                        sub[1]?two=sub[1]:two='';
                        
                        if( _inStr('#',one) && one.length>1 ){
                            code=parseInt( sub[0].replace('#','') );
                            if(typeof(code)==='number'){
                                one=String.fromCharCode(code);
                            }
                        }
                        result+=one+two;
                    }
                    return result;
                }
                else return value;
            };
		   plaintext=convert(plaintext);
		   return plaintext;
		}
	};
	
	//generic onload handler
	
	var _addLoadEvent = function (fn) {
		var UNDEF = "undefined",
			win=window,
			doc=document;
		if (typeof win.addEventListener != UNDEF) {
			win.addEventListener("load", fn, false);
		}
		else if (typeof doc.addEventListener != UNDEF) {
			doc.addEventListener("load", fn, false);
		}
		else if (typeof win.attachEvent != UNDEF) {
			_addListener(win, "load", fn);
		}
		else if (typeof win.onload == "function") {
			var fnOld = win.onload;
			win.onload = function() {
				fnOld();
				fn();
			};
		}
		else {
			win.onload = fn;
		}
	};
    
	var _addListener = function(elem, evtType, callback) {
        if (elem.addEventListener) {
            elem.addEventListener(evtType, callback, false);
        } 
		else if (elem.attachEvent) {
            elem.attachEvent("on" + evtType, callback);
        } 
		else {
            elem["on" + evtType] = callback;
        }
    };
	
    var _removeListener = function(elem, evtType, callback){
        if (elem.removeEventListener) {
          elem.removeEventListener(evtType, callback, false);
        } 
		else if (elem.detachEvent) {
          elem.detachEvent("on" + evtType, callback, false);
        }
		else {
			elem["on" + evtType] = null;
		}
    };
	
	var _getEvent = function(event){
        return event || window.event;
    };
	
	var _getTarget = function(event){
        return event.target || event.srcElement;
    };
	
	//functies vanwege loggerwindow drag
	var _offsetX, _offsetY;
	
	var _startDrag = function(event){
		event=_getEvent(event);
		var t=_getTarget(event);
		
		if(t.id==CNT_DEBUGGER){
			_addListener(t, 'mousemove', _handleMove);
			_addListener(t, 'mouseup', _stopDrag);
			var mouse={};
			mouse.x = event.pageX || event.clientX; 
			mouse.y = event.pageY || event.clientY;
			_offsetX = mouse.x - parseInt(t.style.left);
			_offsetY = mouse.y - parseInt(t.style.top);
		}
		
	};
	
	var _stopDrag = function(event){
		event=_getEvent(event);
		var t=_getTarget(event);
		
		if(t.id==CNT_DEBUGGER){
			_removeListener(t, "mousemove", _handleMove);
			_removeListener(t, "mouseup", _stopDrag);
		}
		
	};
	
	var _handleMove = function(event){
		event=_getEvent(event);
		var t=_getTarget(event);
		
		if(t.id==CNT_DEBUGGER){
			var x,y,mouse={};
			mouse.x = event.pageX || event.clientX; 
			mouse.y = event.pageY || event.clientY;
			x = mouse.x - _offsetX;
			t.style.left = x + "px";
			y = mouse.y - _offsetY;
			t.style.top = y + "px";
		}
	};
	
	var _getViewport = function(e){
		
		var frameWidth,
			frameHeight,
			scroll_left,
			scroll_top;
		
        if (self.innerWidth)
    	{
    		frameWidth = self.innerWidth;
    		frameHeight = self.innerHeight;
    		scroll_left = pageXOffset;
    		scroll_top = pageYOffset;

    	}
    	else if (document.documentElement && document.documentElement.clientWidth)
    	{
    		frameWidth = document.documentElement.clientWidth;
    		frameHeight = document.documentElement.clientHeight;
    		scroll_left = document.documentElement.scrollLeft;
    		scroll_top = document.documentElement.scrollTop;
    		
    	}
    	else if (document.body)
    	{
    		frameWidth = document.body.clientWidth;
    		frameHeight = document.body.clientHeight;
    		scroll_left = document.body.scrollLeft;
    		scroll_top = document.body.scrollTop;
    	}
    	else {return false;}
    	var centerX = frameWidth/2 + scroll_left,
    		centerY = frameHeight/2 + scroll_top,
    		mouse_x=null,
    	    mouse_y=null;
    	
    	if(o.getExactType(e)=='Object'){
    		(e.pageX) ? mouse_x = e.pageX : mouse_x = e.clientX + scrollLeft;
    		(e.pageY) ? mouse_y = e.pageY : mouse_y = e.clientY + scrollTop;
    	}
    	return{
    	   "width"     : frameWidth,
    	   "height"    : frameHeight,
    	   "scrleft"   : scroll_left,
    	   "scrtop"    : scroll_top,
    	   "centX"      : centerX,
    	   "centY"      : centerY,
    	   "mouse_x"	: mouse_x,
    	   "mouse_y"    : mouse_y
    	};
    	
    	
        

	
	
	};
	
	var _setPositionInViewPort = function(element,event,topmargin){
		topmargin=topmargin||0;
		var pos = o.getViewport(event),
			y = pos.mouse_y+25,//add bottomscrollbar height
			x = pos.mouse_x+25,
			marginHor=pos.width-element.innerWidth()+pos.scrleft,
			marginVer=pos.height-element.innerHeight()+pos.scrtop+topmargin;
		
		if(x>marginHor){
			pos.mouse_x -= element.innerWidth();
		}
		if(y>marginVer){
			pos.mouse_y -= element.innerHeight();
		}
		
		return pos;
		
	}
	
	var _mail = function(oJson){
	        if( typeof(oJson)==='string' ){
	        eval("var obj = " + oJson);
	    }
	    else{
	        var obj = oJson;
	    }
	        var mailto_link = 'mailto:'+obj.email+'?subject='+obj.subject+'&body='+obj.message;
	        var win = window.open(mailto_link,'emailWindow', 'left=50000,top=50000,width=0,height=0'); 
            if (win && win.open &&!win.closed){
                win.close();
            }     
	};
    
    var _print = function(elementId){

        var printContent = o.getRef(elementId),
            windowUrl = 'about:blank',
            uniqueName = new Date(),
            windowName = 'Print' + uniqueName.getTime(),
            printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

        printWindow.document.write(printContent.innerHTML);
        printWindow.document.close();
        printWindow.focus();
        printWindow.print();
        printWindow.close();
    };

	
	var _arrayToObj = function(aIn,bsMode /*{boolean}||{string}*/){
		/* array  a[1]='aaaa',  a[2]='bbbb'  etc
		 * bsMode : { 1 : 'aaaa', 2 : 'bbbb'}
		 * reverse : { 'aaaa' : 1,  'bbbb' : 2} equal keys => value with original highest key
         * bsMode = {string}  : { 'aaaa' : {string},  'bbbb' : {string}}
		*/
		
		var len=aIn.length,
			oRet={},
			t=0;
        if(typeof(bsMode)==='undefined'){bsMode=true};
        if(typeof(bsMode)==='boolean'){
            for(t;t<len;t++){
                if(bsMode){
                    oRet[t]=aIn[t];
                }
                else{
                    oRet[ aIn[t] ]=t;
                }
            }
        }
        else{//string,number,object
            for(t;t<len;t++){
                oRet[ aIn[t] ]= bsMode; 
            }
        }
		
		return oRet;
	};
	
	var _objToArray = function(obj){
	    
		var aKey=[],
		    aValue=[],
		    t=0;
		for(name in obj){
			aKey[t]=name;
			aValue[t]=obj[name];
			t++;
		}
		return {"key" : aKey, "val" : aValue};
		
	};
    
    var _arrayMerge = function(destination, source){
        var oSource=_arrayToObj(source),
            oDestination=_arrayToObj(destination);    
        oBject.extend(oDestination, oSource);
        return _objToArray(oDestination).key;
    

    
    };
	
	var _strToObj = function(sIn, sSep, bNumberKey){
		//default separator=" + "
		var aStr=[];
		
		if(sIn==='' || typeof(sIn)!=='string'){
			return {};
		}
		switch(typeof(sSep)){
			case 'boolean' :
				bNumberKey=sSep;
				sSep='+';
				break;
				
			case 'undefined' :
				sSep='+';
				break;
		}
		aStr=sIn.split(sSep);
		return _arrayToObj(aStr, bNumberKey);
	};
	
	var _serialize = function(obj, maxLevels, level){
		    var str = '', 
		        type, 
		        msg;
			// Don't touch, we start iterating at level zero
			if(level == null)  level = 0;
			if(maxLevels == null) maxLevels = 1;
			
			if(maxLevels < 1)     
				return '&error=level_gt_zero';

			// We start with a non null object
			if(obj == null)
			return '&error=object_is_null';
			// End Input Validations

			// Start iterations for all objects in obj
			for(property in obj){
			  try
			  {
				  // Show "property" and "type property"
				  type =  typeof(obj[property]);
                  switch (type) {
                      case 'function' :
                        obj[property]='function';
                        break;
                      case 'string' :
                        
                        break;
                      case 'object' :
        
                        break;
                  }
                  if( !(type=='function' || type=='object' ) ){
                     str += '&' +property + (   (obj[property]==null)?(': =null'):( '='+ obj[property] )   ); 
                  }
                  
				  // We keep iterating if this property is an Object, non null
				  // and we are inside the required number of levels
				  if((type == 'object') && (obj[property] != null) && (level+1 < maxLevels))
				  str += _serialize(obj[property], maxLevels, level+1);
			  }
			  catch(err){
				// Is there some properties in obj we can't access? Print it red.
				if(typeof(err) == 'string') msg = err;
				else if(err.message)        msg = err.message;
				else if(err.description)    msg = err.description;
				else                        msg = 'unknown';

				str += '&error_' + property + '= ' + msg;
			  }
			}
			return str;
		};
		
		
		var _serializePHP = function(sMelding, iLevels){
			var m='', txt, sender, levels,type;
			
			typeof(sSender)== 'undefined' ? sender='' : sender=sSender;
			typeof(iLevels)== 'undefined' ? levels=1 : levels=iLevels;
			
			type=o.getExactType(sMelding);
			
			switch(type){
				case 'String' :
				case 'Number' :
					m=sMelding+'<br><br>';
					break;
				case 'Object' :
					m='<br><br><b> object ['+oBject.length(sMelding)+']</b><br>'
					m+=_inspect(sMelding,levels);//object inspect met maxlevels
					break;
				case 'Undefined' :
					m='type = undefined';
					break;
					
			}
			
			
			
			function _inspect(obj, maxLevels, level){
					  var str = '', type,proptype,exacttype, msg;

						// Start Input Validations
						// Don't touch, we start iterating at level zero
						if(level == null)  level = 0;

						// At least you want to show the first level
						if(maxLevels == null) maxLevels = 1;
						if(maxLevels < 1)     
							return '<font color="red">Error: Levels number must be > 0</font><br><br>';

						// We start with a non null object
						if(obj == null)
						return '<font color="red">Error: Object <b>NULL</b></font><br><br>';
						// End Input Validations
						
						// Each Iteration must be indented
						str += '<ul>';
						// Start iterations for all objects in obj
						for(property in obj){
						  try
						  {
							  // Show "property" and "type property"
							  
								type=o.getExactType(obj[property]);
								var lengte='',display='';
								
								display=obj[property];
								
								if( (type == 'Array') ){
									lengte=' ['+obj[property].length+']';
									display='';
								}
								if( (type == 'Object') ){
									lengte=' ['+oBject.length(obj[property])+']';
									display='';
								}
							  
							  
							  proptype = typeof(property);

							  str += '<li>(' + proptype + ') ' + property + 
									 ( (obj[property]==null)?(': <b>null</b>'):('&nbsp;:<span style="color:green;font-size:0.9em;">&nbsp;('+type+''+lengte+')&nbsp;'+display+'</span>')) + '</li>';

							  // We keep iterating if this property is an Object, non null
							  // and we are inside the required number of levels
							  if((type == 'Object'  || type=='Array') && (obj[property] != null) && (level+1 < maxLevels)) {
								str += _inspect(obj[property], maxLevels, level+1);
							  }
						  }
						  catch(err){
							// Is there some properties in obj we can't access? Print it red.
							if(typeof(err) == 'string') msg = err;
							else if(err.message)        msg = err.message;
							else if(err.description)    msg = err.description;
							else                        msg = 'Unknown';

							str += '<li><font color="red">(Error) ' + property + ': ' + msg +'</font></li>';
						  }
						}

						  // Close indent
						  str += '</ul>';

						return str;
					};

		
			return m;
			
		};
		
		var _getURLbase = function(href){
		
			var host={},
				o={},
				parts,isQ,isH;
			
			isQ=href.indexOf('?');
			if(isQ > -1){
				href=href.substr(0,isQ);
			}
			isH=href.indexOf('#');
			if(isH > -1){
				href=href.substr(0,isH);
			}
			try
			{
				//root
				parts=href.split('/');
				parts.pop();
				o.root=parts.join('/')+'/';
				//protocol
				parts=href.split('//');
				o.protocol=parts[0];
				//host,hostname
				host=parts[1].split('/');
				o.host=host.shift();
				o.hostname=o.host;
				//pathname
				o.pathname='/'+host.join('/');
				//base
				o.base=o.protocol+'//'+o.host+'/';
				return o;
			}
			catch(e){
				return false;
			}
		};
		
	var _getURLparam = function(href,fix){
			//href [#name1=value1&name2=value2] naar object { name1 : value1, name2 : value2 }
			if(!href){
				href=window.location.href;
			}
			var re={},obj={},qu;
			var p={ '?':'#','#':'?'};
			re.resultobject=[];
			if(href.indexOf(fix)> -1){
				qu=href.split(fix)[1];
				if(qu.indexOf(p[fix]) > -1){
					re.resultstring=qu.substr(0,qu.indexOf(p[fix]));
				}
				else{
					re.resultstring=qu;
				}
				obj=o.URLtoObj(re.resultstring);
				oBject.extend(re.resultobject,obj);
			}
			re.href=href;
			return re;
		};
		
		
    var _getGlobals = function(obj){//global object is GL
		   
		    if(typeof(obj)==='undefined'){
		        return GL;
		    }
		    if(obj instanceof Object || obj instanceof Array){
		        var ret={},t=0;
		        
		        for(name in GL){
		            if( typeof(obj[t])!=='undefined'){
		              if(obj[t]!=='>>'){
		                  ret[obj[t]]=GL[name];
		              }
		              t++;
		            }
		            else{
		                ret[name]=GL[name];
		            }
		        }
		        return ret;
		    }
	};
    
    var _setGlobals = function(obj){
        
        for(name in GL){
	    if( typeof(obj[name])!=='undefined'){
	               GL[name]=obj[name];
	           }
	       }
    };
    
    var _getLocalStorage = function(){
    
        if (typeof localStorage == "object"){
            return localStorage;
        } else if (typeof globalStorage == "object"){
            return globalStorage[location.host];
        } else {
            throw new Error("Local storage not available.");
        }
    };
    
    var _getClientKeys = function(seed){
        var globals = _getGlobals(),
            oSafe   = {
                        "useragent"      : o.getUserAgent()  
                      },
        oSafe=oBject.extend(oSafe,globals);
        typeof(seed)!=='undefined' ? oSafe.seed=seed:void(0);
        oSafe.hash=MD5.hex_md5(_serialize(oSafe));
        
        return oSafe;
    };
    
    var _baseConvert = {
    
        d2h : function(j){
        
            var hexchars =  "0123456789ABCDEF";
            var hv = "";
            for (var i=0; i< 4; i++)
              {
                k = j & 15;
                hv = hexchars.charAt(k) + hv;
                j = j >> 4;
              }
            return hv;  
        },

        h2d : function(value){
          var j = value.toUpperCase();
          var d = 0;
          var ch = ' ';
          var hexchars =  "0123456789ABCDEF";
          while (j.length < 4) j = 0 + j;
          for (var i = 0; i < 4; i++)
            {
              ch = j.charAt(i);
              d = d*16 + hexchars.indexOf(ch); 
            }
            return d;
        }
    };
		
	//JSON---------------------------
	var _JSON = {
		
		stringify: function (arg) {
			var c, i, l, s = '', v;

			switch (typeof arg) {
			case 'object':
				if (arg) {
					if (arg.constructor == Array) {
						for (i = 0; i < arg.length; ++i) {
							v = this.stringify(arg[i]);
							if (s) {
								s += ',';
							}
							s += v;
						}
						return '[' + s + ']';
					} else if (typeof arg.toString != 'undefined') {
						for (i in arg) {
							v = arg[i];
							if (typeof v != 'undefined' && typeof v != 'function') {
								v = this.stringify(v);
								if (s) {
									s += ',';
								}
								s += this.stringify(i) + ':' + v;
							}
						}
						return '{' + s + '}';
					}
				}
				return 'null';
			case 'number':
				return isFinite(arg) ? String(arg) : 'null';
			case 'string':
				l = arg.length;
				s = '"';
				for (i = 0; i < l; i += 1) {
					c = arg.charAt(i);
					if (c >= ' ') {
						if (c == '\\' || c == '"') {
							s += '\\';
						}
						s += c;
					} else {
						switch (c) {
							case '\b':
								s += '\\b';
								break;
							case '\f':
								s += '\\f';
								break;
							case '\n':
								s += '\\n';
								break;
							case '\r':
								s += '\\r';
								break;
							case '\t':
								s += '\\t';
								break;
							default:
								c = c.charCodeAt();
								s += '\\u00' + Math.floor(c / 16).toString(16) +
									(c % 16).toString(16);
						}
					}
				}
				return s + '"';
			case 'boolean':
				return String(arg);
			default:
				return 'null';
			}
		},
		parse: function (text,revive) {
			var at = 0,
				ch = ' ';
			
			if(Util.isNullOrEmpty(text)){
				return null;
			}
			
			if(Util.getExactType(text)!=='String'){
				return text;
			}
				
			(typeof(revive)=='undefined')? revive=false:void(0);

			function error(m) {
				throw {
					name: 'JSONError',
					message: m,
					at: at - 1,
					text: text
				};
			}

			function next() {
				ch = text.charAt(at);
				at += 1;
				return ch;
			}

			function white() {
				while (ch) {
					if (ch <= ' ') {
						next();
					} else if (ch == '/') {
						switch (next()) {
							case '/':
								while (next() && ch != '\n' && ch != '\r') {}
								break;
							case '*':
								next();
								for (;;) {
									if (ch) {
										if (ch == '*') {
											if (next() == '/') {
												next();
												break;
											}
										} else {
											next();
										}
									} else {
										error("Unterminated comment");
									}
								}
								break;
							default:
								error("Syntax error");
						}
					} else {
						break;
					}
				}
			}

			function string() {
				var i, s = '', t, u;

				if (ch == '"') {
outer:          while (next()) {
						if (ch == '"') {
							next();
							return s;
						} else if (ch == '\\') {
							switch (next()) {
							case 'b':
								s += '\b';
								break;
							case 'f':
								s += '\f';
								break;
							case 'n':
								s += '\n';
								break;
							case 'r':
								s += '\r';
								break;
							case 't':
								s += '\t';
								break;
							case 'u':
								u = 0;
								for (i = 0; i < 4; i += 1) {
									t = parseInt(next(), 16);
									if (!isFinite(t)) {
										break outer;
									}
									u = u * 16 + t;
								}
								s += String.fromCharCode(u);
								break;
							default:
								s += ch;
							}
						} else {
							s += ch;
						}
					}
				}
				error("Bad string");
			}

			function array() {
				var a = [];

				if (ch == '[') {
					next();
					white();
					if (ch == ']') {
						next();
						return a;
					}
					while (ch) {
						a.push(value());
						white();
						if (ch == ']') {
							next();
							return a;
						} else if (ch != ',') {
							break;
						}
						next();
						white();
					}
				}
				error("Bad array");
			}

			function object() {
				var k, o = {};

				if (ch == '{') {
					next();
					white();
					if (ch == '}') {
						next();
						return o;
					}
					while (ch) {
						k = string();
						white();
						if (ch != ':') {
							break;
						}
						next();
						o[k] = value();
						white();
						if (ch == '}') {
							next();
							return o;
						} else if (ch != ',') {
							break;
						}
						next();
						white();
					}
				}
				error("Bad object");
			}

			function number() {
				var n = '', v;
				if (ch == '-') {
					n = '-';
					next();
				}
				while (ch >= '0' && ch <= '9') {
					n += ch;
					next();
				}
				if (ch == '.') {
					n += '.';
					while (next() && ch >= '0' && ch <= '9') {
						n += ch;
					}
				}
				if (ch == 'e' || ch == 'E') {
					n += 'e';
					next();
					if (ch == '-' || ch == '+') {
						n += ch;
						next();
					}
					while (ch >= '0' && ch <= '9') {
						n += ch;
						next();
					}
				}
				v = +n;
				if (!isFinite(v)) {
					////error("Bad number");
				} else {
					return v;
				}
			}

			function word() {
				switch (ch) {
					case 't':
						if (next() == 'r' && next() == 'u' && next() == 'e') {
							next();
							return true;
						}
						break;
					case 'f':
						if (next() == 'a' && next() == 'l' && next() == 's' &&
								next() == 'e') {
							next();
							return false;
						}
						break;
					case 'n':
						if (next() == 'u' && next() == 'l' && next() == 'l') {
							next();
							return null;
						}
						break;
				}
				error("Syntax error");
			}

			function value() {
				white();
				switch (ch) {
					case '{':
						return object();
					case '[':
						return array();
					case '"':
						return string();
					case '-':
						return number();
					default:
						return ch >= '0' && ch <= '9' ? number() : word();
				}
			}
			
			if(revive){
				return eval( value());
			}
			else {
				return value();
			}
		}
	};
	
//base64
    var _txtToCharCode = function(value){
		var result='',arr,code;
		
        arr = value.split('');
        for(key in arr){
            code=arr[key].charCodeAt(0);
            if( code==32||(code>47&&code<58)||(code>67&&code<91)||(code>96&&code<123) ){//only A-Z, a-z, 0-9, space
                result+=arr[key];
            }
            else{
                result+='&#'+arr[key].charCodeAt(0)+';';
            }
        }
		return result;
    };
    
    var _charcodeToTxt = function(value){
		var result='',arr,sub,one,two,code;
		
        arr = value.split('&');
        for(key in arr){
        
            sub=arr[key].split(';');
            sub[0]?one=sub[0]:one='';
            sub[1]?two=sub[1]:two='';
            
            if( _inStr('#',one) && one.length>1 ){
                code=parseInt( sub[0].replace('#','') );
                if(typeof(code)==='number'){
                    one=String.fromCharCode(code);
                }
            }
            result+=one+two;
        }
		return result;
    };

	var _base64 = {
	
		keyStr : "ABCDEFGHIJKLMNOP" +
				 "QRSTUVWXYZabcdef" +
				 "ghijklmnopqrstuv" +
				 "wxyz0123456789+/" +
				 "=",

	   encode : function(input,safe) {
		  var output = "";
          if(input==='' || input===null || typeof(input)==='undefined'){
              return '';
          }
		  var chr1, chr2, chr3 = "";
		  var enc1, enc2, enc3, enc4 = "";
		  var i = 0;
          if(safe){
              input=_txtToCharCode(input);
          }
		  do {
			 chr1 = input.charCodeAt(i++);
			 chr2 = input.charCodeAt(i++);
			 chr3 = input.charCodeAt(i++);

			 enc1 = chr1 >> 2;
			 enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			 enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			 enc4 = chr3 & 63;

			 if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			 } else if (isNaN(chr3)) {
				enc4 = 64;
			 }

			 output = output +
				this.keyStr.charAt(enc1) +
				this.keyStr.charAt(enc2) +
				this.keyStr.charAt(enc3) +
				this.keyStr.charAt(enc4);
			 chr1 = chr2 = chr3 = "";
			 enc1 = enc2 = enc3 = enc4 = "";
		  } while (i < input.length);

		  return output;
	   },

	   decode : function(input, safe) {
			  var output = "";
              if(input==='' || input===null || typeof(input)==='undefined'){
                  return '';
              }
			  var chr1, chr2, chr3 = "";
			  var enc1, enc2, enc3, enc4 = "";
			  var i = 0;

			  // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
			  var base64test = /[^A-Za-z0-9\+\/\=]/g;
			  if (base64test.exec(input)) {
				 alert("There were invalid base64 characters in the input text.\n" +
					   "Valid base64 characters are A-Z, a-z, 0-9, +, /, and =\n" +
					   "Expect errors in decoding.");
			  }
			  input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

			 do 
			 {
				 enc1 = this.keyStr.indexOf(input.charAt(i++));
				 enc2 = this.keyStr.indexOf(input.charAt(i++));
				 enc3 = this.keyStr.indexOf(input.charAt(i++));
				 enc4 = this.keyStr.indexOf(input.charAt(i++));

				 chr1 = (enc1 << 2) | (enc2 >> 4);
				 chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				 chr3 = ((enc3 & 3) << 6) | enc4;

				 output = output + String.fromCharCode(chr1);

				 if (enc3 != 64) {
					output = output + String.fromCharCode(chr2);
				 }
				 if (enc4 != 64) {
					output = output + String.fromCharCode(chr3);
				 }

				 chr1 = chr2 = chr3 = "";
				 enc1 = enc2 = enc3 = enc4 = "";

			} while (i < input.length);
            
            if(safe){
                output=_charcodeToTxt(output);
            }
			return output;
		}
	};
	
	var _uuid = function(){	
		
		  var s = [], itoh = '0123456789ABCDEF';
		 
		  // Make array of random hex digits. The UUID only has 32 digits in it, but we
		  // allocate an extra items to make room for the '-'s we'll be inserting.
		  for (var i = 0; i <36; i++) s[i] = Math.floor(Math.random()*0x10);
		 
		  // Conform to RFC-4122, section 4.4
		  s[14] = 4;  // Set 4 high bits of time_high field to version
		  s[19] = (s[19] & 0x3) | 0x8;  // Specify 2 high bits of clock sequence
		 
		  // Convert to hex chars
		  for (var i = 0; i <36; i++) s[i] = itoh[s[i]];
		 
		  // Insert '-'s
		  s[8] = s[13] = s[18] = s[23] = '-';
		 
		  return s.join('');
	
	};
	
	var _createXHR = function(){
            if (typeof XMLHttpRequest != "undefined"){
                _createXHR = function(){
                    return new XMLHttpRequest();
                };
            } else if (typeof ActiveXObject != "undefined"){
                _createXHR = function(){                    
                    if (typeof arguments.callee.activeXString != "string"){
                        var versions = ["MSXML2.XMLHttp.6.0", "MSXML2.XMLHttp.3.0",
                                        "MSXML2.XMLHttp"];
                
                        for (var i=0,len=versions.length; i < len; i++){
                            try {
                                var xhr = new ActiveXObject(versions[i]);
                                arguments.callee.activeXString = versions[i];
                                return xhr;
                            } catch (ex){
                                //skip
                            }
                        }
                    }
                
                    return new ActiveXObject(arguments.callee.activeXString);
                };
            } else {
                _createXHR = function(){
                    throw new Error("No XHR object available.");
                };
            }
            
            return _createXHR();
    };
	
	var _postLoggerData = function(url,data,mode){
		var xhr = _createXHR();
		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4) {
				if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
					xhr.data=xhr.responseText;
					return xhr;
				}
				else {
					xhr.error=xhr.status;
					return xhr;
				}
			}            
        };
		  
        xhr.open("post", url, true);
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
		data=o.safeHTML(data);
        xhr.send("mode="+mode+"&data="+data);
	};
	
	var _postData = function(url,data,mode){
		typeof(mode)=='undefined'?mode='get':void(0);
		typeof(data)=='undefined'?data=null:void(0);
		var xhr = _createXHR(),
			send,
			obj=xhr;
		xhr.onreadystatechange = function (event) {
			if (xhr.readyState == 4) {
				if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
					xhr.data=xhr.responseText;
					return xhr;
				}
				else{
					xhr.error=xhr.status;
					return xhr;
				}
			}            
        };
		 
        xhr.open(mode, url, true);
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		if(data===null){
			send=null;
		}
		else{
			data=o.safeHTML(data);
			send="data="+data;
		}
        xhr.send(send);
	};
	//cookies
	var _cookie = {

		get: function (name){
			var cookieName = encodeURIComponent(name) + "=",
				cookieStart = document.cookie.indexOf(cookieName),
				cookieValue = null;
				
			if (cookieStart > -1){
				var cookieEnd = document.cookie.indexOf(";", cookieStart);
				if (cookieEnd == -1){
					cookieEnd = document.cookie.length;
				}
				cookieValue = decodeURIComponent(document.cookie.substring(cookieStart + cookieName.length, cookieEnd));
			} 

			return cookieValue;
		},
		
		set: function (name, value, expires, path, domain, secure) {
			var cookieText = encodeURIComponent(name) + "=" + encodeURIComponent(value);
		
			if (expires instanceof Date) {
				cookieText += "; expires=" + expires.toGMTString();
			}
		
			if (path) {
				cookieText += "; path=" + path;
			}
		
			if (domain) {
				cookieText += "; domain=" + domain;
			}
		
			if (secure) {
				cookieText += "; secure";
			}
		
			document.cookie = cookieText;
			return cookieText;
		},
		
		unset: function (name, path, domain, secure){
			this.set(name, "", new Date(0), path, domain, secure);
			return null;
		}

	};
	
	var _subcookie = {

		get: function (name, subName){
			var subCookies = this.getAll(name);
			if (subCookies){
				return subCookies[subName];
			} else {
				return null;
			}
		},
		
		getAll: function(name){
			var cookieName = encodeURIComponent(name) + "=",
				cookieStart = document.cookie.indexOf(cookieName),
				cookieValue = null,
				result = {};
				
			if (cookieStart > -1){
				var cookieEnd = document.cookie.indexOf(";", cookieStart);
				if (cookieEnd == -1){
					cookieEnd = document.cookie.length;
				}
				cookieValue = document.cookie.substring(cookieStart + cookieName.length, cookieEnd);
				
				if (cookieValue.length > 0){
					var subCookies = cookieValue.split("&");
					
					for (var i=0, len=subCookies.length; i < len; i++){
						var parts = subCookies[i].split("=");
						result[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
					}
		
					return result;
				}  
			} 

			return null;
		},
		
		set: function (name, subName, value, expires, path, domain, secure) {
		
			var subcookies = this.getAll(name) || {};
			subcookies[subName] = value;
			this.setAll(name, subcookies, expires, path, domain, secure);

		},
		
		setAll: function(name, subcookies, expires, path, domain, secure){
		
			var cookieText = encodeURIComponent(name) + "=";
			var subcookieParts = new Array();
			
			for (var subName in subcookies){
				if (subName.length > 0 && subcookies.hasOwnProperty(subName)){
					subcookieParts.push(encodeURIComponent(subName) + "=" + encodeURIComponent(subcookies[subName]));
				}
			}
			
			if (subcookieParts.length > 0){
				cookieText += subcookieParts.join("&");
					
				if (expires instanceof Date) {
					cookieText += "; expires=" + expires.toGMTString();
				}
			
				if (path) {
					cookieText += "; path=" + path;
				}
			
				if (domain) {
					cookieText += "; domain=" + domain;
				}
			
				if (secure) {
					cookieText += "; secure";
				}
			} else {
				cookieText += "; expires=" + (new Date(0)).toGMTString();
			}
		
			document.cookie = cookieText;        
			return cookieText;
		},
		
		unset: function (name, subName, path, domain, secure){
			var subcookies = this.getAll(name);
			if (subcookies){
				delete subcookies[subName];
				this.setAll(name, subcookies, null, path, domain, secure);
			}
		},
		
		unsetAll: function(name, path, domain, secure){
			this.setAll(name, null, new Date(0), path, domain, secure);
		}

	};
    
    var _inStr = function(n,h){
        return h.indexOf(n)!=-1;
    };
    
    var _inArray = function(n,h){
        var r=false;
        for(k in h){
            if(n===h[k]) r=true;
        }
        return r;
    };
    
    var _apprise = function(string, args, detail, callback){
    	if(Util.getscriptUriParam('alert')!=='true'){
			return;
		}
    	var default_args =
    		{
    		'confirm'		:	false, 		// Ok and Cancel buttons
    		'verify'		:	false,		// Yes and No buttons
    		'input'			:	false, 		// Text input (can be true or string for default text)
    		'animate'		:	false,		// Groovy animation (can true or number, default is 400)
    		'message'		:   true,
    		'success'		:   false,
    		'warning'		:   false,
    		'textOk'		:	'Ok',		// Ok button default text
    		'textCancel'	:	'Annuleren',	// Cancel button default text
    		'textYes'		:	'Ja',		// Yes button default text
    		'textNo'		:	'Nee'		// No button default text
    		}
    	
    	if(args) 
    		{
    		for(var index in default_args) 
    			{ if(typeof args[index] == "undefined") args[index] = default_args[index]; }
    		}
    	
    	var aHeight = $(document).height();
    	var aWidth = $(document).width();
    	var widgetStyle = 'apprise-message';
    	if(args['success']){
    		widgetStyle = 'apprise-success';
    	}
    	
    	if(args['warning']){
    		widgetStyle = 'apprise-warning';
    	}
    	
    	if(args['error']){
    		widgetStyle = 'apprise-error';
    	}
    	
    	$('body').append('<div class="appriseOverlay" id="aOverlay"></div>');
    	
    	Util.setStyle('aOverlay','height', aHeight);
    	Util.setStyle('aOverlay','width', aWidth);
    	Util.doShow('aOverlay');
    	
    	
    	
    	$('body').append('<div class="appriseOuter '+widgetStyle+'" id="aOuter"></div>');
    	$('.appriseOuter').append('<div class="appriseInner '+widgetStyle+'" id="aInner"></div>');
    	$('.appriseInner').append(string);
    	if(detail !== null){
    		$('.appriseInner').append('<div class="appriseCollapsible">'+
    				'<i id="showDetail" style="cursor:pointer"></i>'+
    				'<p id="appriseDetail" style="display:none">'+detail+'</p></div>');
    	}
    	var pos=_getViewport();
    	
    	$('#showDetail').addClass('glyphicon glyphicon-menu-down');
    	
    	$('#showDetail').click(function(){
    		if($('#showDetail').hasClass('glyphicon glyphicon-menu-down')){
    			Util.doShow('appriseDetail');
    			$('#showDetail').removeClass('glyphicon glyphicon-menu-down');
    			$('#showDetail').addClass('glyphicon glyphicon-menu-up');
    		}else{
    			Util.doHide('appriseDetail');
    			$('#showDetail').removeClass('glyphicon glyphicon-menu-up');
    			$('#showDetail').addClass('glyphicon glyphicon-menu-down');
    		}
    	});
    	
    	
		
        //Util.setStyle('aOuter','left', (pos.width - 400 )/2 - pos.scrleft);
    	//Util.setStyle('aOuter','top', (pos.height - $('.appriseOuter').height() )/2 - pos.scrtop);
    	
    	//using scrollposition does not work
    	Util.setStyle('aOuter','left', (pos.width - 400 )/2);
    	Util.setStyle('aOuter','top', (pos.height - $('.appriseOuter').height() )/2);
    	
    	Util.doShow('aOuter');

        if(args)
        	{
        	if(args['input'])
        		{
        		if(typeof(args['input'])=='string')
        			{
        			$('.appriseInner').append('<div class="aInput"><input type="text" class="aTextbox" t="aTextbox" value="'+args['input']+'" /></div>');
        			}
        		else
        			{
    				$('.appriseInner').append('<div class="aInput"><input type="text" class="aTextbox" t="aTextbox" /></div>');
    				}
    			$('.aTextbox').focus();
        		}
        	}

        $('.appriseInner').append('<div class="aButtons"></div>');
        if(args)
        	{
    		if(args['confirm'] || args['input'])
    			{ 
    			$('.aButtons').append('<button value="ok">'+args['textOk']+'</button>');
    			$('.aButtons').append('<button value="cancel">'+args['textCancel']+'</button>'); 
    			}
    		else if(args['verify'])
    			{
    			$('.aButtons').append('<button value="ok">'+args['textYes']+'</button>');
    			$('.aButtons').append('<button value="cancel">'+args['textNo']+'</button>');
    			}
    		else
    			{ $('.aButtons').append('<button value="ok">'+args['textOk']+'</button>'); }
    		}
        else
        	{ $('.aButtons').append('<button value="ok">OK</button>'); }
    	
    	$(document).keydown(function(e) 
    		{
    		if($('.appriseOverlay').is(':visible'))
    			{
    			if(e.keyCode == 13) 
    				{ $('.aButtons > button[value="ok"]').click(); }
    			if(e.keyCode == 27) 
    				{ $('.aButtons > button[value="cancel"]').click(); }
    			}
    		});
    	
    	var aText = $('.aTextbox').val();
    	if(!aText) { aText = false; }
    	$('.aTextbox').keyup(function()
        	{ aText = $(this).val(); });
       
        $('.aButtons > button').click(function()
        	{
        	$('.appriseOverlay').remove();
    		$('.appriseOuter').remove();
        	if(callback)
        		{
    			var wButton = $(this).attr("value");
    			if(wButton=='ok')
    				{ 
    				if(args)
    					{
    					if(args['input'])
    						{ callback(aText); }
    					else
    						{ callback(true); }
    					}
    				else
    					{ callback(true); }
    				}
    			else if(wButton=='cancel')
    				{ callback(false); }
    			}
    		});
        
        
  
    };
    
    /**
     * Searches in a list of objects the resultValue for the provided  queryKey where the whereKey = whereValue
     */
    var _findInObject = function(obj,whereKey, equalsValue, resultKey){
    	
    	for (name in obj) {

    		for (key in obj[name]) {
    			
    			if (key == whereKey && obj[name][key] == equalsValue) {
    				if(typeof(resultKey)=='undefined' || resultKey==null){
    					return obj[name];
    				}
    				return obj[name][resultKey];
    			}
    		}
    	}
    	return null;
	};
	
	
	 /**
     * Searches in a list of objects for the provided value
     */
    var _getObjectByValue = function(obj,value){
    	
    	for(i=0;i<obj.length;i++){
    		
    		if(obj[i].value == value){
    			return obj[i];
    		}
    	}
    	return null;
	};
    
    
    /**
     * Show an animated loader indicator
     */
    var _busyIndicator = function(headerTxt,footerTxt,show){
    	
    	if(headerTxt==null || typeof(headerTxt)==='undefined'){
    		headerTxt='';
    	}
    	
    	if(footerTxt==null || typeof(footerTxt)==='undefined'){
    		footerTxt = '';
    	}
 
    	if(show){
	    	var aHeight = $(document).height();
	    	var aWidth = $(document).width();
	    	
	    	$('body').append('<div class="busyOverlay" id="bOverlay"></div>');
	    	
	    	Util.setStyle('bOverlay','height', aHeight);
	    	Util.setStyle('bOverlay','width', aWidth);
	    	Util.doShow('bOverlay');
	    	
	    	$('body').append('<div class="busyOuter" id="bOuter"></div>');
	    	$('.busyOuter').append('<div class="b-header">'+headerTxt+'</div>');
	    	$('.busyOuter').append('<img src="'+IMG_AJAXLOADER+'"/>');
	    	$('.busyOuter').append('<div class="b-footer">'+footerTxt+'</div>');
	        
	    	var pos=_getViewport();
	    	//Util.echo('_busyIndicator line 2223', pos,3)
			
	        Util.setStyle('bOuter','left', (pos.width - 50 )/2 - pos.scrleft);
	    	Util.setStyle('bOuter','top', (pos.height - $('.busyOuter').height() )/2);
	    	Util.doShow('bOuter');
    	
    	}else{
    		$('.busyOverlay').remove();
    		$('.busyOuter').remove();
    	}

    };
    
    
    /**
     * Show a centered info message
     */
    var _info = function(headerTxt,footerTxt,showTime){
    	
    	if(headerTxt==null || typeof(headerTxt)==='undefined'){
    		headerTxt='';
    	}
    	
    	if(footerTxt==null || typeof(footerTxt)==='undefined'){
    		footerTxt = '';
    	}
    	
    	if(showTime==null || typeof(showTime)==='undefined'){
    		showTime=3000;
    	}
    	
    	var aHeight = $(document).height();
    	var aWidth = $(document).width();
    	
    	$('body').append('<div class="infoOverlay" id="infOverlay"></div>');
    	
    	Util.setStyle('infOverlay','height', aHeight);
    	Util.setStyle('infOverlay','width', aWidth);
    		
	    $('body').append('<div class="infoOuter" id="infOuter"></div>');
	    $('.infoOuter').append('<div class="inf-header">'+headerTxt+'</div>');
	    $('.infoOuter').append('<div class="inf-footer">'+footerTxt+'</div>');
	        
	    var pos=_getViewport();
			
	    Util.setStyle('infOuter','left', (pos.width - $('.infoOuter').width() )/2 - pos.scrleft);
	    Util.setStyle('infOuter','top', (pos.height - $('.infoOuter').height() )/2);
	    
	    $('.infoOverlay').fadeIn(500);
	    $('.infoOuter').fadeIn(500);
	    
	    //Util.doShow('infOverlay');
	    //Util.doShow('infOuter');
    	
	    window.setTimeout(function(){
	    	
	    	$('.infoOverlay').fadeOut(500, function(){
	    		$('.infoOverlay').remove();
	    	});
	    	$('.infoOuter').fadeOut(500, function(){
	    		$('.infoOuter').remove();
	    	});

	    }, showTime);
    	
    	
    	

    };
    
    var _parseBoolean = function(test){
    	
    	if(test==null || typeof(test)=='undefined'){
    		return false;
    	}
    	switch(o.getExactType(test)){
	    	case 'String':
	    		switch (String(test).toLowerCase()) {
				    case "true":
				    case "1":
				    case "yes":
				    case "y":
				      return true;
				    case "false":
				    case "0":
				    case "no":
				    case "n":
				      return false;
				    default:
				      //you could throw an error, but 'undefined' seems a more logical reply
				      return undefined;
			  }
	    		break;
	    	case 'Number':
	    		switch(test){
		    		case 0:
		    			return false;
		    		case 1:
		    			return true;
		    		default :
		    			return undefined
	    		}
	    		break;
	    	case 'Boolean':
	    		return test;
	    	default:
	    		return undefined;	
    	}		
    }
    
	//=======================================================
	//PUBLIC METHODS
	
	var o = {
    
        GL : {},//global object
        
        onDocumentReady	: function(fn){ _addLoadEvent(fn); },
        
        addListener		: function(elem, evtType, callback){
        	return _addListener(elem, evtType, callback);
        },
        
    	
    	removeListener	: function(elem, evtType, callback){
        	return _removeListener(elem, evtType, callback);
        },
      
        getEvent			: function(event){
    	   return _getEvent(event);
        },
    	   	
    	getTarget : function(event){
            return _getTarget(event);
        },
	
		getVal 		: function(id){ return this.getRef(id).value; },
		
		setVal 		: function(id,cn){ this.getRef(id) ? this.getRef(id).value=cn : void(0); },
		
		setHTML		: function(id,cn){ this.getRef(id) ? this.getRef(id).innerHTML=cn : void(0); },
		
		getHTML		: function(id){ 
							if(this.getRef(id)){
								return this.getRef(id).innerHTML;
							}
							else {
								return false;
							}
					},
					
		getRef 		: function(obj){
							var i,form,forms;
							if(typeof obj == "string"){
								obj=obj.replace('#','');
                                
                                if(_inStr('.',obj)){
                                    obj=obj.replace('.','');
                                    forms = window.document.forms;
                                    for (i = 0; i < forms.length; i++) {
                                        form = forms[i];
                                        if(form.className===obj){
                                            return form;
                                            break;
                                        }
                                    }
                                    return false;
                                }
								if( document.getElementById(obj)){
									return document.getElementById(obj);
								}
								else if(document.getElementsByName(obj)[0]){
											return document.getElementsByName(obj)[0];
									  }
									  else if(document.getElementsByTagName(obj)[0]){
											return document.getElementsByTagName(obj)[0];
									  }
									  else {
											return false;
									  }
							} 
							else {
								return obj;
							}
					  },
		
		setStyle	: function(obj,style,value){
		                
		                if(typeof(value)==='number'){
		                    value+='px';
		                }
                        try{
                            this.getRef(obj).style[style]= value;
                        } catch(e){}
					  },
					  
		getStyle	: function(obj,style){
                        try{
                            return this.getRef(obj).style[style];
                        }
                        catch(e){}
					  },
		
		doShow		: function(o,time){
						var that=this;
						var show=function(){ that.setStyle(o,'display','block');};
						var hide=function(){ that.doHide(o);};
				
						if(time)
						{
							show();
							setTimeout(hide,time);
						} else {
							show();
						}
					  },
			
		doHide		: function(o){
						this.setStyle(o,'display','none');
					  },
					  
		doShowHide	: function(o,flgVisible){//visible
						if(flgVisible){
							if( this.getStyle(o,'visibility')=='' || this.getStyle(o,'visibility')=='visible'  ){
								this.setStyle(o,'visibility','visible');
							}
							else{
								this.setStyle(o,'visibility','hidden');
							}
						}
						else{
							if( this.getStyle(o,'display')=='' || this.getStyle(o,'display')=='none'  ){
								this.setStyle(o,'display','block');
							}
							else{
								this.setStyle(o,'display','none');
							}
						}
					  },
					  
	    setClassName	: function(o,className){
							this.getRef(o).className=className;
						  },
					  
		addClassName	: function(o,className){
							var el = this.getRef(o);
							var old = el.className;
							el.className = old+" "+className;
					      },
					  
		removeClassName	: function(o,className){
							var el = this.getRef(o);
							var old = el.className;
							
							el.className=old.replace(className,"");
						  },
					  
					 
					  
		addStylesheet : function(url){
							if(document.createStyleSheet) {
								document.createStyleSheet(url);
							}
							else {
								var sheet=document.createElement('link');
								sheet.rel='stylesheet';
								sheet.href=url;
								this.getRef('head').appendChild(sheet);
							}
					  },
					  
		setFocus	: function(o) {
						this.getRef(o).focus();
					  },
					  
		selectText	: function(textbox, startIndex, stopIndex) {
							var textbox=this.getRef(textbox);
							if(!stopIndex){
								var stopIndex=parseInt(textbox.value.length);
							}
							if (textbox.setSelectionRange){
								textbox.setSelectionRange(startIndex, stopIndex);
							} else if (textbox.createTextRange){
								var range = textbox.createTextRange();
								range.collapse(true);
								range.moveStart("character", startIndex);
								range.moveEnd("character", stopIndex - startIndex);
								range.select();                    
							}
							textbox.focus();
						},
					  
		//======== helpers ===========

		posX	 	: function(obj){
						var o = this.getRef(obj),
							curleft = 0;

						  if (o.offsetParent)  {
							while (o.offsetParent) {
							  curleft += o.offsetLeft;
							  o = o.offsetParent;
							}
						  }
						  else if (o.x){
							curleft += o.x;
						  }
						  return curleft;
		},

		posY		: function(obj) {
						var o = this.getRef(obj),
							curtop = 0;

						  if (o.offsetParent)  {
							while (o.offsetParent) {
							  curtop += o.offsetTop;
							  o = o.offsetParent;
							}
						  }
						  else if (o.y){
							curtop += o.y;
						  }
						  return curtop;
		},
							
        getViewport : function(e){
            return _getViewport(e);
        },
        
        setPositionInViewPort : function(element,event,topmargin){
        	return _setPositionInViewPort(element,event,topmargin);
        },

		URIencode : function (string) {
			return escape(_utf8.encode(string));
		},

		URIdecode : function (string) {
			return _utf8.decode(unescape(string));
		},
		
		encode_utf8 : function(s){
			return _utf8.encode(s);
		},
		
		decode_utf8 : function(s){
			return _utf8.decode(s);
		},
		
		base64encode : function(str,safe){
			return _base64.encode(str,safe);
		},
		
		base64decode : function(str,safe){
			return _base64.decode(str,safe);
		},
        
        txtToCharCode : function(txt){
            return _txtToCharCode(txt);
        },
        
        charCodeToTxt : function(code){
            return _charcodeToTxt(code);
        },

		safeHTML : function(html){
			return _base64.encode( _utf8.encode(html) );
		},

		safeURI : function(uri,mode){
            typeof(mode)==='undefined'||mode==='' ||!mode ? mode='false':void(0);
			uri=this.URLtoObj(uri);//make object
			uri=_JSON.stringify(uri);//object to string
            switch(mode){
                case 'base64':
                    uri=_base64.encode(uri);//base64encode
                    break;
                case 'base64safe':
                    uri=_base64.encode(uri,true);//base64encode
                    break;
            }
			return uri;
		},
		
		safeObjToURI : function(obj,mode){
			typeof(mode)==='undefined'||mode==='' ||!mode ? mode='base64':void(0);
			var uri=_JSON.stringify(obj);//object to string
			switch(mode){
	            case 'base64':
	                uri=_base64.encode(uri);//base64encode
	                break;
	            case 'base64safe':
	                uri=_base64.encode(uri,true);//base64encode
	                break;
	        }
			return uri;
		},
		
		isArray : function(array){ 
			return !!(array && array.constructor == Array); 
		},
		
		isNumber : function(x){ 
			return ( (typeof x === typeof 1) && (null !== x) && isFinite(x) );
		},
		
		toInteger : function(x){
			
			 if(Util.isNullOrEmpty(x)){
				 return null;
			 }
	         var p = parseInt(x);
	         if( !isNaN(p) && this.isNumber(p) ){
	        	 return p;
	         }
	         this.error('Util::PARSE_ERROR', 'Kon ['+x+'] niet parsen naar een geldige integer') ;
	         return null;
        
		},
		
		toFloat : function(x){
			 x = x.replace(",",".");
	         var p = parseFloat(x);
	         if( !isNaN(p) && this.isNumber(p) ){
	        	 return p;
	         }
	         throw 'Util::PARSE_ERROR', 'Kon ['+x+'] niet parsen naar een geldige float';
	         return null;
       
		},
		
		in_str : function(needle,haystack){
			return _inStr(needle,haystack);
		},
        
        in_array : function(needle,haystack){
            return _inArray(needle,haystack);
        },
        
        isNullOrEmpty : function(test){
        	if(test===null || typeof(test)==='undefined' ||  test==='' ||   test==='null' ||   test==='NULL' || typeof(test)==='NaN'){
        		return true;
        	}
        	if(this.getExactType(test) == 'Array' && test.length==0){
        		return true;
        	}
        	
        	if(this.getExactType(test) == 'Object' && Object.keys(test).lenghth==0){
        		return true;
        	}
        	return false;
        },
        
        isNotNullOrEmpty : function(test){	
        	return !Util.isNullOrEmpty(test);
        },
		
		getExactType : function(obj) {
			var nativeType=(typeof(obj)).slice(0,1).toUpperCase() + (typeof(obj)).slice(1);
			try {
				if (obj.constructor) {
					var strType;
					strType = obj.constructor.toString().match(/function (\w*)/)[1];
					if (strType.replace(' ','') == '') strType = 'n/a';			//Mozilla needs this
					if ( ! (strType in oc(['Array','Boolean','Date','Enumerator','Error','Function','Number','RegExp','String','VBArray'])) ) {
						strType = 'Object';
					}
					return strType;
				} else {//native
					return nativeType;
				}
			} catch(e) {
				return nativeType;
			}
			
			function oc(a) {
				var o = {};
				for(var i=0;i<a.length;i++) {
					o[a[i]]='';
				}
				return o;
			}
		},
		
		objToArray : function(oInput){
		    return _objToArray(oInput);
		},

		arrayToObj : function(aInput,bNumberKey){//returns {"key" : aKey, "val" : aValue}
			return _arrayToObj(aInput,bNumberKey);
		},
        
        array_merge : function(destination, source){
            return _arrayMerge(destination, source);
        },
	
		strToObj : function(sInput, sSeparator, bNumberKey){
			return _strToObj(sInput, sSeparator, bNumberKey);
		},
		
		removeHTMLTags : function(strInputCode){
				var result;
				strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1){
					return (p1 == "lt")? "<" : ">";
				});
				result=strInputCode;
				result=result.replace(/(&nbsp;)/g, ' ');
				result=result.replace(/<ul>/g, '\n');
				result=result.replace(/<li>/g, '\t');
				result=result.replace(/<\/li>/g, '\n');
				result=result.replace(/<\/ul>/g, '\n\n');
				result=result.replace(/<\/?[^>]+(>|$)/g, "");	
				
				return result;	
		},

        /*
stringFormat example:
document.write( String.format( "no tokens<br />" ) );
document.write( String.format( "one token, no args ({0})<br />" ) );
document.write( String.format( "one token, one args ({0})<br />", "arg1" ) );
document.write( String.format( "one tokens, two args ({0})<br />", "arg1", "arg2" ) );
document.write( String.format( "two tokens, two args ({0},{1})<br />", "arg1", "arg2" ) );
document.write( String.format( "two tokens swapped, two args ({1},{0})<br />", "arg1", "arg2" ) );
document.write( String.format( "four tokens interwoven, two args ({0},{1},{0},{1})<br />", "arg1", "arg2" ) );
*/

        stringFormat : function( text ){

            if ( arguments.length <= 1 ){
                return text;
            }
            var tokenCount = arguments.length - 2;
            for( var token = 0; token <= tokenCount; token++ ){
                text = text.replace( new RegExp( "\\{" + token + "\\}", "gi" ),arguments[ token + 1 ] );
            }
            return text;
        },
		
		intOnly : function(event){
			var key;
			var keychar;
			 
			if (window.event){
			   key = window.event.keyCode;
			}
			else if(e){
			   key = event.which;
			}
			else{
			   return true;
			}
			keychar = String.fromCharCode(key);
			
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) ){
			   return true;
			}
			else if ((("0123456789").indexOf(keychar) > -1)){
			   return true;
			}
			else{
			   return false;
			}
		},
		
		goTo : function (url,get,d){
			if(get){
				var location=this.getLocation();
				if( url.indexOf('/') == -1){
					url=location.root+url;
				}
				if(!d){
					url=url+'?d=';
				}
				url=url+get;
			}
			window.location.replace(url);
        },

		winOpen : function (param){
		    var obj={  root        : '',
		               url         : '',
		               name        : 'win_'+Math.floor(Math.random()*10000),
		               options     : 'menubar=1,resizable=1,scrollbars=1,width=600,height=850'
		        };
			if(obj.root==''){	
				obj.root=_getURLbase(obj.url);
			}
		    oBject.extend(obj,param);
	        return window.open(obj.root+obj.url ,obj.name,obj.options);
        },
        
        
        getRoot : function(){
			var d=document.location,
				aUrl={};
			
			aUrl=d.pathname.split('/');
			aUrl.pop();
			
			d.root=d.protocol+'//'+d.host+aUrl.join('/')+'/';
			return d;
			
		},
		
		getQuery : function(href){
			var re={};
			if(href.indexOf('?')> -1){
				var qu=href.split('?')[1].split('&');
				var le,js;
				le=qu.length;
				for(var t=0;t<=le;t++){
					if(qu[t]){
					js=qu[t].split('=');
					re[js[0]]=js[1];
					}
				}
			}
			re.href=href;
			return re;
		},
		
        //print content of element
        printThis : function(id){
            _print(id);
        },
		
		getLocation : function(href,param){
			var d={},
				loc=document.location,
				part,
				hs,
				qu,
				ur={};
			
			if(href){
				d.href=href;
			}
			else{
				d.href=loc.href;
			}
			//searchobject en hashobject
			qu = _getURLparam(d.href,'?');
			hs = _getURLparam(d.href,'#');
			ur = _getURLbase(d.href);
			
			if(href){
				
				d.protocol=ur.protocol;
				d.host=ur.host;
				d.hostname=ur.hostname;
				d.pathname=ur.pathname;
				d.port=loc.port;
				d.reload=loc.reload;
				d.replace=loc.replace;
				d.assign=loc.assign;
				d.hash='#'+hs.resultstring;
				d.search='?'+qu.resultstring;
			}
			else{
				d=loc;
			}
			//root
			
			
			//base
			d.root=ur.root;
            d.base=ur.base;
			d.hashobject=hs.resultobject;
			d.searchobject=qu.resultobject;
			
			if(param){
				return d[param];
			}
			else {
				return d;
			}
			
		},
		
		objToKey : function(obj){
			var aKey=[],t=0;
			for(name in obj){
				aKey[t]=name;
				t++;
			}
			return aKey;
		},
		
		
		objFlip : function(obj){
			var oRev={},
				type=this.getExactType(obj);
				
			switch(type){
				case 'Number' :
				case 'String' :
					oRev[obj]=0;
					break;
					
				case 'Array'  :
				case 'Object' :
					for(var key in obj){
						oRev[obj[key]]=key;
			
					}
					break;
				default 	  :
					return obj;
					break;
			
			}
			
			return oRev;
		},
		
		objToURL : function(obj, maxLevels, level) {
	       return _serialize(obj, maxLevels, level);
		},
		
		URLtoObj : function(url, sep1, sep2){
			var re={},qu,le,js,arr=[];
			if(typeof(sep1)==='string') url=url.split(sep1)[1];
			url=url.replace('?','&');
			url=url.replace('#','&');
            url=decodeURIComponent(url);
			qu=url.split('&');
			le=qu.length;
			
			for(var t=0;t<=le;t++){
				if(qu[t] && qu[t].indexOf('=')> -1){
					js=qu[t].split('=');
					if(js[1] && typeof(sep2)==='string' && js[1].indexOf(sep2)> -1){
						js[1]=js[1].split(sep2);
					}
                    //array detect
                    if(js[1] && js[0].indexOf('[]')> -1){
                        js[0]=js[0].split('[')[0];
                        
                        if(typeof( arr[js[0]])==='undefined'){
                            arr[js[0]]= [];
                        }
                        arr[js[0]].push(js[1]);
                        js[0]=null;   
                    }
                    if(js[0]){
                        re[js[0]]=js[1];
                    } 
				}  
			}
            //add arrayfields
            for(name in arr){
                re[name]=arr[name];
            }
			return re;
		},
		
		serializePHP :function(obj, maxLevels){
		
			return _serializePHP(obj, maxLevels);
		},
		
		JSONparse : function(str,revive){
			return _JSON.parse(str,revive);
		},
		
		JSONtoString : function(obj){
			return _JSON.stringify(obj);
		},
		
		//cookies--------------------------------------------
		setCookie : function(name, value, expires, path, domain, secure) {
			return _cookie.set(name, value, expires, path, domain, secure);
		},

		getCookie : function(name) {
			return _cookie.get(name);
		},

		unsetCookie : function(name, path, domain, secure) {
			return _cookie.unset(name, path, domain, secure);
		},
		
		setSubcookie : function(name, subName, value, expires, path, domain, secure){
			_subcookie.set(name, subName, value, expires, path, domain, secure);
		},
		
		setallSubcookie : function(name, subcookies, expires, path, domain, secure){
			return _subcookie.setAll(name, subcookies, expires, path, domain, secure);
		},
		
		getSubcookie : function(name, subName){
			return _subcookie.get(name, subName);
		},
		
		getallSubcookie : function(name){
			return _subcookie.getAll(name);
		},
		
		unsetSubcookie : function(name, subName, path, domain, secure){
			_subcookie.unset(name, subName, path, domain, secure);
		},
		
		unsetallSubcookie : function(name, path, domain, secure){
			_subcookie.unsetAll(name, path, domain, secure);
		},
		
		getGlobals : function(obj){
		    return _getGlobals(obj);
		},
        
        setGlobals : function(obj){
		    return _setGlobals(obj);
		},
        
        getClientKeys : function(seed){
            return _getClientKeys(seed);
        },
		
		getUserAgent : function(){
	       return navigator.userAgent.replace(/ /gi,"").toLowerCase(); 
	    },
        
        decToHex : function(value){
            return _baseConvert.d2h(value);
        },
        
        hexToDec : function(value){
            return _baseConvert.h2d(value);
        },
        
        /**
		* Create and return a "version 4" RFC-4122 UUID string.
		*/
        createUUID : function(){
        	return _uuid();
        },
	
		numberToCurrency : function (fnumber,separator){
			var num = (fnumber+'').replace(',','.');
			num = Math.round(num*100)/100;
			var numText = num + '';
			if((num) == Math.round(num)){
				//No decimals? Add .00
				numText += ".00";
			}
			else if((num*10) == Math.round(num*10)){
				//If 1 decimal only, add 0
				numText += "0";
			}
			if(separator===','){
			    return numText.replace('.',',');;
			}
			else{
			     return numText;
			}
		},
		
		mailTo : function (obj) {
		    //alert(obj);
	       _mail(obj);
		},
        
        getLocalStorage : function(){
            return _getLocalStorage();
        },
		
		objXHR : function(){
			return _createXHR();
		},
		
		XHRsubmit : function(){
			return _postData();
		},
		
		getscriptUriParam : function(/*string*/ paramKey){
			if(typeof(paramKey)==='undefined' || paramKey===null){
				alert('getscriptUriParam ERROR: provided key cannot be <null>');
				return null;
			}
			var scriptURL;
			var scripts = document.getElementsByTagName('script');
			//get the util.js uri
			for (var key in scripts) {
				if (scripts.hasOwnProperty(key)) {
					if(scripts[key]!==null && typeof(scripts[key])!=='undefined' && scripts[key].src!==null && typeof(scripts[key].src)!=='undefined' &&scripts[key].src.indexOf('utils/util.js') > -1 ){
						scriptURL = scripts[key].src;
						
					}
				}
			}

			if(typeof(scriptURL)=='undefined'){
				return null;
			}
			if ( scriptURL.indexOf('?') > -1 ) {
				//url parameter found
				if ( scriptURL.indexOf('&') > -1 ) {
					//more then one key/valuepair given
					var params = scriptURL.split('?')[1].split('&');
					
					for (var key in params) {
						if (params.hasOwnProperty(key)) {
						   var param = params[key].split('=');
						      if(paramKey===param[0]){
						    	  return param[1];
						      }
						   }
					}
					//key not found
					return null; 
				}else{
					//only one key/valuepair  given
					var param = scriptURL.split('?')[1].split('=');
					if(paramKey===param[0]){
						return param[1];
					}else{
						return null
					}
				}
				
				return param;
				
			} else {
				//no uri parameters given
				return null;
			}
			
		},
	   
		//loggerfuncties---------------------------------------
		
		assert : function(oCondition, sMelding){
			var m=sMelding;
			if(!oCondition){
				this.log(m);
			}
		},
		
		/*
		 * --------------------------------------------------------------------------------------------
		 * Alert uses apprise library
		 * {
    		'confirm'		:	false, 		// Ok and Cancel buttons
    		'verify'		:	false,		// Yes and No buttons
    		'input'			:	false, 		// Text input (can be true or string for default text)
    		'animate'		:	false,		// Groovy animation (can true or number, default is 400)
    		'textOk'		:	'Ok',		// Ok button default text
    		'textCancel'	:	'Cancel',	// Cancel button default text
    		'textYes'		:	'Yes',		// Yes button default text
    		'textNo'		:	'No'		// No button default text
    		}
		 */
		
		//debug helper method
		ho : function(message){
			message = '<span class="appriseTitle">SCRIPT HALT</span><hr>' + (typeof(message)=='undefined'?'':message);
			_apprise(message, {message:true},null, null);
		},
		
		alert : function(title,message,callback){
			message = '<span class="appriseTitle">'+title+':</span><hr>' + message;
			_apprise(message, {message:true},null, callback);
		},
		
		success : function(title,message,callback){
			message = '<span class="appriseTitle">'+title+':</span><hr>' + message;
			_apprise(message, {success:true},null, callback);
		},
		
		warning : function(title,message,callback){
			message = '<span class="appriseTitle">'+title+':</span><hr>' + message;
			_apprise(message, {warning:true}, null,callback);
		},
		
		error : function(title,message, detail, callback){
			message = '<span class="appriseTitle">'+title+':</span><hr>' + message+(detail?'. Klik voor meer informatie':'');
			_apprise(message, {error:true}, detail, callback);
		},
		
		confirm : function(title,message,callback,btnOK,btnCancel){
			message = '<span class="appriseTitle">'+title+':</span><hr>' + message;
			btnOK = btnOK || 'OK';
			btnCancel = btnCancel || 'Annuleren';
			_apprise(message, {message:true,confirm:true,textOk:btnOK,textCancel:btnCancel}, null, callback);
		},
		
		confirmWarn : function(title,message,callback){
			message = '<span class="appriseTitle">'+title+':</span><hr>' + message;
			_apprise(message, {warning:true,confirm:true,}, null,callback);
		},
			
		verify : function(title,message,callback){
			message = '<span class="appriseTitle">'+title+':</span><hr>' + message;
			_apprise(message, {verify:true,message:true}, null,callback);
		},
			
		input : function(title,message,callback){
			message = '<span class="appriseTitle">'+title+':</span><hr>' + message;
			_apprise(message, {input:true,message:true}, null, callback);
		},
		
		
		showBusyIndicator : function(headerTxt,footerTxt){
			if(headerTxt==null || typeof(headerTxt)==='undefined'){
	    		headerTxt='Even geduld aub';
	    	}
	    	
	    	if(footerTxt==null || typeof(footerTxt)==='undefined'){
	    		footerTxt = 'wordt geladen...';
	    	}
			_busyIndicator(headerTxt,footerTxt,true);
		},
		
		hideBusyIndicator : function(){
			_busyIndicator('','',false);
		},
		
		findInObject : function(obj,whereKey, equalsValue, resultKey){
			return _findInObject(obj,whereKey, equalsValue, resultKey);
		},
		
		getObjectByValue : function(obj,value){
			return _getObjectByValue(obj,value);
		},
		
		ellipsis : function(txt,length){
			if(typeof(txt)=='undefined' || this.isNullOrEmpty(txt)){
				return '';
			}
			if(txt.length>length+3){
				return txt.substring(0,length)+'...';
			}
			return txt;
			
		},

      die : function(message){
         message = message?message:'';
         console.log(''+message);
         alert('DIE'+ (message));
      },
		
      /**
       * 	-Only Select statements are supported
			-The requested fields may be a "*" or a list of fields. "*" is likely faster in most cases.
			-When typing lists "select field1,field2,field3" or "limit 5,10 do not use spaces.
			-When using the "where" clause enclose all conditions with one set of parenthesis "where (category=='The Category' || category=='Other Category')".
			-The where clause is a javascript condition, not sql. It should use the scope emplied by "from". 
				Javascript functions may be used here as well as javascript operators.
			-The from clause should establish the scope you would like returned. 
				It should start with "json" and use the dot notation: "json.channel.items" and should point to an array within the object.
			-The order by option can accept a list but will only order by the first field at this time(asc,desc,ascnum,descnum).
			
			Example:
			$.jsonsql.query("select title,url from json.channel.items where (category=='javascript' || category=='vista') order by title,category asc limit 3",json); 
       */
	  jsonsql : {
				
				query: function(sql,json){

					var returnfields = sql.match(/^(select)\s+([a-z0-9_\,\.\s\*]+)\s+from\s+([a-z0-9_\.]+)(?: where\s+\((.+)\))?\s*(?:order\sby\s+([a-z0-9_\,]+))?\s*(asc|desc|ascnum|descnum)?\s*(?:limit\s+([0-9_\,]+))?/i);
					
					var ops = { 
						fields: returnfields[2].replace(' ','').split(','), 
						from: returnfields[3].replace(' ',''), 
						where: (returnfields[4] == undefined)? "true":returnfields[4],
						orderby: (returnfields[5] == undefined)? []:returnfields[5].replace(' ','').split(','),
						order: (returnfields[6] == undefined)? "asc":returnfields[6],
						limit: (returnfields[7] == undefined)? []:returnfields[7].replace(' ','').split(',')
					};

					return this.parse(json, ops);		
				},
				
				parse: function(json,ops){
					var o = { fields:["*"], from:"json", where:"", orderby:[], order: "asc", limit:[] };
					for(i in ops) o[i] = ops[i];

					var result = [];		
					result = this.returnFilter(json,o);
					result = this.returnOrderBy(result,o.orderby,o.order);
					result = this.returnLimit(result,o.limit);
							
					return result;
				},
				
				returnFilter: function(json,jsonsql_o){
					
					var jsonsql_scope = eval(jsonsql_o.from);
					var jsonsql_result = [];
					var jsonsql_rc = 0;

					if(jsonsql_o.where == "") 
						jsonsql_o.where = "true";

					for(var jsonsql_i in jsonsql_scope){
						with(jsonsql_scope[jsonsql_i]){
							if(eval(jsonsql_o.where)){
								jsonsql_result[jsonsql_rc++] = this.returnFields(jsonsql_scope[jsonsql_i],jsonsql_o.fields);
							}
						}
					}
					
					return jsonsql_result;
				},
				
				returnFields: function(scope,fields){
					if(fields.length == 0)
						fields = ["*"];
						
					if(fields[0] == "*")
						return scope;
						
					var returnobj = {};
					for(var i in fields)
						returnobj[fields[i]] = scope[fields[i]];
					
					return returnobj;
				},
				
				returnOrderBy: function(result,orderby,order){
					if(orderby.length == 0) 
						return result;
					
					result.sort(function(a,b){	
						switch(order.toLowerCase()){
							case "desc": return (eval('a.'+ orderby[0] +' < b.'+ orderby[0]))? 1:-1;
							case "asc":  return (eval('a.'+ orderby[0] +' > b.'+ orderby[0]))? 1:-1;
							case "descnum": return (eval('a.'+ orderby[0] +' - b.'+ orderby[0]));
							case "ascnum":  return (eval('b.'+ orderby[0] +' - a.'+ orderby[0]));
						}
					});

					return result;	
				},
				
				returnLimit: function(result,limit){
					switch(limit.length){
						case 0: return result;
						case 1: return result.splice(0,limit[0]);
						case 2: return result.splice(limit[0]-1,limit[1]);
					}
				}
				
		},
		
		/**
		 * @return boolean TRUE if the string starts with the needle string
		 */
		stringStartsWith : function (hayStack, needle) {          
			hayStack = hayStack || "";
	        if (needle.length > hayStack.length)
	            return false;
	        return hayStack.substring(0, needle.length) === needle;
	    },
	    
	    parseBoolean : function(input){
		    return _parseBoolean(input);
		},
		
		
		postByFormSubmit : function(path, params, method) {
		    method = method || "post"; // Set method to post by default if not specified
		    
		    var form = document.createElement("form");
		    form.setAttribute("method", method);
		    form.setAttribute("action", path);

		    for(var key in params) {
		    	
		        if(params.hasOwnProperty(key)) {
		        	
		            var hiddenField = document.createElement("input");
		            hiddenField.setAttribute("type", "hidden");
		            hiddenField.setAttribute("name", key);
		            hiddenField.setAttribute("value", params[key]);

		            form.appendChild(hiddenField);
		         }
		    }

		    document.body.appendChild(form);
		    form.submit();
		},
		
	
		display : function(title, message){
			
			if(this.getscriptUriParam('display')!=='true'){
				return;
			}
			var WIDGET = 'util_display-widget';
			var widget = document.createElement('div');
			var html='';
			var cnt_display = this.getRef(CNT_DISPLAY_MESSAGE);
			
			widget.setAttribute('class',WIDGET);
			widget.style.display='none';
			
			html+='<span class="title">'+title+'</span>';
			html+='<span class="message">'+message+'</span>';
			this.setHTML(widget, html);
			cnt_display.appendChild(widget);
			this.doShow(widget,3000);
			this.doShow(cnt_display,4000);
			
		},
		
		/**
		 * show an info container centered in the viewport
		 */
		info : function(title,message,showtime){
			
			_info(title,message,showtime);
		},
		
		/**
		 * Return an object dump as HTML
		 */
		logObject : function(object, level){
			if(!level)level=2;
			var cntDisplay = 'cnt_display';
			var message = '<br><br>';
			message+='<i class="glyphicon glyphicon-eye-open pointer" onclick="Util.doShowHide(\''+cntDisplay+'\')"></i>';
			message+='<div id="cnt_display" style="display:none">';
			message+=oBject.inspect(object,level,0,{})
			message+='</div>';
			return message;
		},
		
		
		
		
		logInit : function(sMode){//to do : loggerOptions object
			if(sMode=='console'){
				_is_onscreen=false;
				_is_popup=false;
				_is_remote=false;
			}
			if(sMode=='remote'){
				_is_remote=true;
			}
			if(sMode=='popup'){
				_is_onscreen=false;
				_is_popup=true;
				_postLoggerData( ROOT+UTIL+"logger/setLoggerData.php",'','clear' );//CLEAR LOGGERFILE to do; vanuit loggerOptions
				this.setCookie('logger','init');
			}
			var body = this.getRef('body'),
				div = document.createElement('div'),
				cnt_display = document.createElement('div'),
				show = document.createElement('div'),
				close = document.createElement('img'),
				clear = document.createElement('img');
			div.setAttribute('id', CNT_DEBUGGER);
			cnt_display.setAttribute('id',CNT_DISPLAY_MESSAGE);
			show.setAttribute('id', CNT_DEBUGGER_TXT);
			div.style.display='none';
			cnt_display.style.display='none';
			
			if(sMode=='relative'){
				div.style.position='relative';
				div.style.marginTop='20px';
				div.style.marginLeft='100px';
				div.style.color='black';
			}
			else {
				div.style.position='absolute';
				div.style.zIndex='1000000';
				div.style.top='100px';
				div.style.left='50px';
				div.style.color='black';
				
				div.onmousedown=function(event){
					_startDrag(event);
				}
			}
			cnt_display.style.position='absolute';
			cnt_display.style.zIndex='1000000';
			cnt_display.style.bottom='0px';
			cnt_display.style.right='20px';
			cnt_display.style.width='220px';
			cnt_display.style.height='100%';
			
			close.onclick=function(){
							Util.doHide(CNT_DEBUGGER);
						}
			clear.onclick=function(){
							Util.doHide(CNT_DEBUGGER);
							Util.setHTML(CNT_DEBUGGER_TXT, '');	
						}
			body.appendChild(div);
			body.appendChild(cnt_display);
			
			close.setAttribute('src', IMG_DEBUGGER_CLOSE);
			clear.setAttribute('src', IMG_DEBUGGER_CLEAR);
            close.setAttribute('alt', '[ close ]');
			clear.setAttribute('alt', '[close and clear]  ');
		
			div.appendChild(close);
			div.appendChild(clear);
			div.appendChild(show);
			
			_is_debugger=true;
			
			this.addStylesheet(LOGGERSTYLE_URL);
			this.addStylesheet(ALERTSTYLE_URL);
		},
        
        logger  : {
        
            LOGGERDEFAULTCOLLAPSE :[
                'parentNode',
                'childNodes',
                'classList',
                'attributes',
                'offsetParent',
                'children',
                'style',
                'ownerDocument',
                'lastChild',
                'firstChild',
                'previousSibling',
                'firstElementChild',
                'lastElementChild',
                'previousElementSibling',
                'nextElementSibling',
                'nextSibling',
                '0','1','2','3','4','5','6','7','8','9'
            ],
            
            showHide : function(id){
                o.doShowHide(id);
                var btn = 'btn_'+id;
                    txt = o.getHTML(btn);
                if(txt==='[+]&nbsp;expand&nbsp;'){
                    o.setHTML(btn,'[-]collapse');
                }
                else{
                    o.setHTML(btn,'[+]&nbsp;expand&nbsp;');
                }
            }
        
        }
		
	};
	return o;
})();




//OBJECT EXTENTIONS EN TOOLS
var oBject = (function(){

	var _object = function(o){
            function F(){}
            F.prototype = o;
            return new F();
    };
    
    var _index = 0;
        
	var o = {
			
		clone : function(original){
			return _object(original);
		},
	
		extend : function(destination, source) {
		  for (property in source) {
			destination[property] = source[property];
		  }
		  return destination;
		},

		inspect : function(obj, maxLevels, level, oDisplay,hidden,property,first){
		  var str = '', type, msg;

			// Start Input Validations
			// Don't touch, we start iterating at level zero
			if(level == null)  level = 0;
            _index++;
            //extend here oDisplay with default collapsed item-array ['firstChild','lastChild' etc..]
            oDisplay=Util.array_merge(oDisplay, Util.logger.LOGGERDEFAULTCOLLAPSE);
            //alert(oDisplay);
			// At least you want to show the first level
			if(maxLevels == null) maxLevels = 1;
			if(maxLevels < 1)     
				return '<font color="red">Error: Levels number must be > 0</font><br><br>';

			// We start with a non null object
			if(obj == null)
			return '<font color="red">Error: Object <b>NULL</b></font><br><br>';
			// End Input Validations

			// Each Iteration must be indented
            if(hidden){
                str += '<ul id="'+property+first+'_'+(level-1)+(_index)+'" class="toggle" style="display:none">';
            }
            else{
                str += '<ul>';
            }
			

			// Start iterations for all objects in obj
			for(property in obj){
			  try {
				// Show "property" and "type property"
							  
				type=Util.getExactType(obj[property]);
				var lengte='',display='';
								
				display=obj[property];
                
				level===0?first=property:void(0);				
				if( (type == 'Array') ){
					lengte=' ['+obj[property].length+']';
					display='';
					}
					if( (type == 'Object') ){
						lengte=' ['+oBject.length(obj[property])+']';
						display='';
					}
					if( (type == 'Function') ){
						display='';
					}
                   
                    if(typeof(oDisplay)!=='undefined' && Util.in_array(property,oDisplay ) && type!=='Function'){
                        if(level<maxLevels-1){
                            display='   <i id="btn_'+property+first+'_'+level+(_index+1)+'" style="text-decoration:underline" onclick="Util.logger.showHide(\''
                                        +property+first+'_'+level+(_index+1)+'\')">[+]&nbsp;expand&nbsp;</i>'; 
                        }
                        else{
                            display=''; 
                        }
                        hidden=true;
                    }
                    else{
                        hidden=false;
                    }
                    
					str += '<li>' + property + ( (obj[property]==null)?(': <b>null</b>'):('&nbsp;:<span style="color:green;font-size:0.9em;">&nbsp;('+
                            type+''+lengte+')&nbsp;'+display+'</span>')) + '</li>';

					// We keep iterating if this property is an Object, non null
					// and we are inside the required number of levels
					
                    if((type == 'Object'  || type=='Array' ) && (obj[property] != null) && (level+1 < maxLevels)) {
						str += this.inspect(obj[property], maxLevels, level+1, oDisplay,hidden,property,first);
                    }
			}
			catch(err){
				// Is there some properties in obj we can't access? Print it red.
				if(typeof(err) == 'string') msg = err;
				else if(err.message)        msg = err.message;
				else if(err.description)    msg = err.description;
				else                        msg = 'Unknown';

				str += '<li><font color="red">(Error) ' + property + ': ' + msg +'</font></li>';
			  }
			}

			  // Close indent
			  str += '</ul>';

			return str;
		},
		
		inheritPrototype : function(subType, superType){
			var prototype = _object(superType.prototype);   //create object
			prototype.constructor = subType;               //augment object
			subType.prototype = prototype;                 //assign object
		},
		
		length :  function(obj) {
			var size = 0, key;
			for (key in obj) {
				//if (obj.hasOwnProperty(key)) size++;
				size++;
			}
			return size;
		}
	
	};
	
	return o;

})();



Util.cookies = ( function()
{
	var resolveOptions, assembleOptionsString, parseCookies, constructor, defaultOptions = {
		expiresAt: null,
		path: '/',
		domain:  null,
		secure: false
	};
	/**
	* resolveOptions - receive an options object and ensure all options are present and valid, replacing with defaults where necessary
	*
	* @access private
	* @static
	* @parameter Object options - optional options to start with
	* @return Object complete and valid options object
	*/
	resolveOptions = function( options )
	{
		var returnValue, expireDate;

		if( typeof options !== 'object' || options === null )
		{
			returnValue = defaultOptions;
		}
		else
		{
			returnValue = {
				expiresAt: defaultOptions.expiresAt,
				path: defaultOptions.path,
				domain: defaultOptions.domain,
				secure: defaultOptions.secure
			};

			if( typeof options.expiresAt === 'object' && options.expiresAt instanceof Date )
			{
				returnValue.expiresAt = options.expiresAt;
			}
			else if( typeof options.hoursToLive === 'number' && options.hoursToLive !== 0 )
			{
				expireDate = new Date();
				expireDate.setTime( expireDate.getTime() + ( options.hoursToLive * 60 * 60 * 1000 ) );
				returnValue.expiresAt = expireDate;
			}

			if( typeof options.path === 'string' && options.path !== '' )
			{
				returnValue.path = options.path;
			}

			if( typeof options.domain === 'string' && options.domain !== '' )
			{
				returnValue.domain = options.domain;
			}

			if( options.secure === true )
			{
				returnValue.secure = options.secure;
			}
		}

		return returnValue;
		};
	/**
	* assembleOptionsString - analyze options and assemble appropriate string for setting a cookie with those options
	*
	* @access private
	* @static
	* @parameter options OBJECT - optional options to start with
	* @return STRING - complete and valid cookie setting options
	*/
	assembleOptionsString = function( options )
	{
		options = resolveOptions( options );

		return (
			( typeof options.expiresAt === 'object' && options.expiresAt instanceof Date ? '; expires=' + options.expiresAt.toGMTString() : '' ) +
			'; path=' + options.path +
			( typeof options.domain === 'string' ? '; domain=' + options.domain : '' ) +
			( options.secure === true ? '; secure' : '' )
		);
	};
	/**
	* parseCookies - retrieve document.cookie string and break it into a hash with values decoded and unserialized
	*
	* @access private
	* @static
	* @return OBJECT - hash of cookies from document.cookie
	*/
	parseCookies = function()
	{
		var cookies = {}, i, pair, name, value, separated = document.cookie.split( ';' ), unparsedValue;
		for( i = 0; i < separated.length; i = i + 1 )
		{
			pair = separated[i].split( '=' );
			name = pair[0].replace( /^\s*/, '' ).replace( /\s*$/, '' );

			try
			{
				value = decodeURIComponent( pair[1] );
			}
			catch( e1 )
			{
				value = pair[1];
			}

			if( typeof JSON === 'object' && JSON !== null && typeof JSON.parse === 'function' )
			{
				try
				{
					unparsedValue = value;
					value = JSON.parse( value );
				}
				catch( e2 )
				{
					value = unparsedValue;
				}
			}

			cookies[name] = value;
		}
		return cookies;
	};

	constructor = function(){};

	/**
	 * get - get one, several, or all cookies
	 *
	 * @access public
	 * @paramater Mixed cookieName - String:name of single cookie; Array:list of multiple cookie names; Void (no param):if you want all cookies
	 * @return Mixed - Value of cookie as set; Null:if only one cookie is requested and is not found; Object:hash of multiple or all cookies (if multiple or all requested);
	 */
	constructor.prototype.get = function( cookieName )
	{
		var returnValue, item, cookies = parseCookies();

		if( typeof cookieName === 'string' )
		{
			returnValue = ( typeof cookies[cookieName] !== 'undefined' ) ? cookies[cookieName] : null;
		}
		else if( typeof cookieName === 'object' && cookieName !== null )
		{
			returnValue = {};
			for( item in cookieName )
			{
				if( typeof cookies[cookieName[item]] !== 'undefined' )
				{
					returnValue[cookieName[item]] = cookies[cookieName[item]];
				}
				else
				{
					returnValue[cookieName[item]] = null;
				}
			}
		}
		else
		{
			returnValue = cookies;
		}

		return returnValue;
	};
	/**
	 * filter - get array of cookies whose names match the provided RegExp
	 *
	 * @access public
	 * @paramater Object RegExp - The regular expression to match against cookie names
	 * @return Mixed - Object:hash of cookies whose names match the RegExp
	 */
	constructor.prototype.filter = function( cookieNameRegExp )
	{
		var cookieName, returnValue = {}, cookies = parseCookies();

		if( typeof cookieNameRegExp === 'string' )
		{
			cookieNameRegExp = new RegExp( cookieNameRegExp );
		}

		for( cookieName in cookies )
		{
			if( cookieName.match( cookieNameRegExp ) )
			{
				returnValue[cookieName] = cookies[cookieName];
			}
		}

		return returnValue;
	};
	/**
	 * set - set or delete a cookie with desired options
	 *
	 * @access public
	 * @paramater String cookieName - name of cookie to set
	 * @paramater Mixed value - Any JS value. If not a string, will be JSON encoded; NULL to delete
	 * @paramater Object options - optional list of cookie options to specify
	 * @return void
	 */
	constructor.prototype.set = function( cookieName, value, options )
	{
		if( typeof options !== 'object' || options === null )
		{
			options = {};
		}

		if( typeof value === 'undefined' || value === null )
		{
			value = '';
			options.hoursToLive = -8760;
		}

		else if( typeof value !== 'string' )
		{
			if( typeof JSON === 'object' && JSON !== null && typeof JSON.stringify === 'function' )
			{
				value = JSON.stringify( value );
			}
			else
			{
				throw new Error( 'cookies.set() received non-string value and could not serialize.' );
			}
		}


		var optionsString = assembleOptionsString( options );

		document.cookie = cookieName + '=' + encodeURIComponent( value ) + optionsString;
	};
	/**
	 * del - delete a cookie (domain and path options must match those with which the cookie was set; this is really an alias for set() with parameters simplified for this use)
	 *
	 * @access public
	 * @paramater MIxed cookieName - String name of cookie to delete, or Bool true to delete all
	 * @paramater Object options - optional list of cookie options to specify ( path, domain )
	 * @return void
	 */
	constructor.prototype.del = function( cookieName, options )
	{
		var allCookies = {}, name;

		if( typeof options !== 'object' || options === null )
		{
			options = {};
		}

		if( typeof cookieName === 'boolean' && cookieName === true )
		{
			allCookies = this.get();
		}
		else if( typeof cookieName === 'string' )
		{
			allCookies[cookieName] = true;
		}

		for( name in allCookies )
		{
			if( typeof name === 'string' && name !== '' )
			{
				this.set( name, null, options );
			}
		}
	};
	/**
	 * test - test whether the browser is accepting cookies
	 *
	 * @access public
	 * @return Boolean
	 */
	constructor.prototype.test = function()
	{
		var returnValue = false, testName = 'cT', testValue = 'data';

		this.set( testName, testValue );

		if( this.get( testName ) === testValue )
		{
			this.del( testName );
			returnValue = true;
		}

		return returnValue;
	};
	/**
	 * setOptions - set default options for calls to cookie methods
	 *
	 * @access public
	 * @param Object options - list of cookie options to specify
	 * @return void
	 */
	constructor.prototype.setOptions = function( options )
	{
		if( typeof options !== 'object' )
		{
			options = null;
		}

		defaultOptions = resolveOptions( options );
	};

	return new constructor();
} )();

var GL = {};//global interface object
var $U = {};//utility shortcut
