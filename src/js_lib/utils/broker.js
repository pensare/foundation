function EventBroker(id){
    this.producers = {};
    this.consumers = {};
    this.identifier=id;//publisher and / or subscriber id
}

EventBroker.prototype = {
    constructor: EventBroker,

    addProducer: function(type, subscriber){
        if (typeof this.producers[type] == "undefined"){
            this.producers[type] = [];
        }
        if(subscriber){
            this.producers[type].push(subscriber);
        
        }
    },
    
    publish: function(event){
        if (!event.target){
            event.target = this;
        }
        if (this.producers[event.type] instanceof Array){
            var producers = this.producers[event.type];
            for (var i=0, len=producers.length; i < len; i++){
                producers[i](event);
            }
        }            
    },

    removeProducer: function(type, subscriber){
        if (this.producers[type] instanceof Array){
            var producers = this.producers[type];
            for (var i=0, len=producers.length; i < len; i++){
                if (producers[i] === subscriber){
                    break;
                }
            }
            
            producers.splice(i, 1);
        }            
    },
    
    createConsumer : function(obj){
            if(!obj){
                obj={};
            }
            
            if(!obj.consumerID){
                alert("createSubscriber Error : No consumer ID definded");
            }
            
            if (typeof this.consumers[obj.consumerID] == "undefined"){
            this.consumers[obj.consumerID] = [];
        }

        
            
            if(!obj.callback){
            //generic callback, show warning
            var id = this.identifier;
                obj.callback= function(event){
                    alert('WARNING : NO CALLBACK DEFINED for '+id+' consumer ');
                };
            }
            this.consumers[obj.consumerID].push(obj);
            return obj.callback;
    },
        
    destroyConsumer: function(id){
       //TODO remove consumer with id = id from array  
    }
};